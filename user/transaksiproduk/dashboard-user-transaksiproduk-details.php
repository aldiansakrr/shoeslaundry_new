<?php 
$id = $_SESSION["user"];
$user = query("SELECT * FROM user WHERE id_user = $id")[0];

$idTransaction = $_GET["id_transaksiproduk"];
$transaction_details = query("SELECT * FROM detail_transaksiproduk INNER JOIN transaksi_produk ON detail_transaksiproduk.id_transaksiproduk = transaksi_produk.id_transaksiproduk INNER JOIN produk ON detail_transaksiproduk.id_produk = produk.id_produk WHERE detail_transaksiproduk.id_transaksiproduk = $idTransaction");
$transaction_jasa = query("SELECT * FROM detail_transaksiproduk INNER JOIN transaksi_produk ON detail_transaksiproduk.id_transaksiproduk = transaksi_produk.id_transaksiproduk INNER JOIN jasa ON detail_transaksiproduk.id_jasa = jasa.id_jasa WHERE detail_transaksiproduk.id_transaksiproduk = $idTransaction");
// var_dump($transaction_details);
?>
  <nav
  class="navbar navbar-expand-lg navbar-light navbar-store fixed-top"
  data-aos="fade-down"
>
  <div class="container-fluid">
    <button
      class="btn btn-secondary d-md-none mr-auto mr-2"
      id="menu-toggle"
    >
      &laquo; Menu
    </button>
    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarResponsive"
    >
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collpase navbar-collapse" id="navbarResponsive">
      <!-- dekstop menu -->
      <ul class="navbar-nav d-none d-lg-flex ml-auto">
        <li class="nav-item dropdown">
          <a
            href="#"
            class="nav-link"
            id="navbarDropdown"
            role="button"
            data-toggle="dropdown"
          >
            <img
              src="../assets/images/person-circle.svg"
              alt="profile"
              height="40px"
              class="rounded-circle mr-2 profile-picture"
            />
            <?php 
              $id_user = $_SESSION['user'];
              $user = query("SELECT * FROM user WHERE id_user = $id_user")[0];
            ?>
            Hi, <?= $user["nama"]; ?>
          </a>
          <div class="dropdown-menu">
            <a href="../index.php" class="dropdown-item">Back To Home</a>
            <div class="dropdown-divider"></div>
            <a href="../logout.php" class="dropdown-item">logout</a>
          </div>
        </li>
      </ul>

      <!-- mobile app -->
      <ul class="navbar-nav d-block d-lg-none">
        <li class="nav-item">
          <a href="" class="nav-link"> Hi, <?= $user["nama"]; ?></a>
        </li>
      </ul>
    </div>
  </div>
</nav>
  <div class="section-content section-dashboard-home" data-aos="fade-up">
    <div class="container-fluid">
      <div class="dashboard-heading">
      <?php 
        $transactionMain = query("SELECT * FROM transaksi_produk WHERE id_transaksiproduk = $idTransaction")[0];
      ?>
        <h2 class="dashboard-title">#<?= $transactionMain["code"]; ?></h2>
        <p class="dashboard-subtitle">Transactions Details</p>
      </div>
      <div class="dashboard-content store-cart">
        <div class="row" data-aos="fade-up" data-aos-delay="100">
            <div class="col-12 table-responsive">
              <table class="table table-borderless table-cart">
                <thead>
                  <tr>
                    <th>Image</th>
                    <th>Name &amp; Seller</th>
                    <th>Kode Produk</th>
                    <th>Banyak</th>
                    <th>Price</th>
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody>
                <?php 
                $banyak = 0;
                $total = 0;
                ?>
                  <?php if(isset($transaction_details)){ foreach ($transaction_details as $t) : ?>
                    <?php 
                      $idProduct = $t["id_produk"];
                      $product = query("SELECT * FROM produk WHERE produk.id_produk = $idProduct");
                    //   $gallery = query("SELECT * FROM products_galleries INNER JOIN products ON products_galleries.product_id = products.id_product WHERE products_galleries.product_id = $idProduct");
                      $banyak += $t["jumlah"];
                      $total = $t["harga"] * $t["jumlah"];
                    ?>
                    <tr>
                      <td style="width: 10%;">
                        <img src="../admin/produk/<?= $t["foto_produk"]; ?>" alt="" class="cart-image w-100">
                      </td>
                      <td style="width: 20%;">
                        <div class=""><?= $t["nama_produk"]; ?></div>
                        <!-- <div class="product-title"><?= $t["unit"] / 1000; ?> Kilogram</div> -->
                      </td>
                      <td style="width: 20%;">
                        <div class="">#<?= $t["kode_produk"]; ?></div>
                      </td>
                      <td style="width: 10%;">
                        <div class=""><?= $t["jumlah"]; ?></div>
                      </td>
                      <td style="width: 20%;">
                        <div class="">Rp. <?= number_format($t["harga"]); ?></div>
                        <div class="product-title">IDR</div>
                      </td>
                      <td style="width: 20%;">
                        <div class="">Rp. <?= number_format($total); ?></div>
                        <div class="product-title">IDR</div>
                      </td>
                    </tr>
                  <?php endforeach; } ?>
                  <?php if(isset($transaction_jasa)){ foreach ($transaction_jasa as $tj) : ?>
                    <?php 
                      $idJasa = $tj["id_jasa"];
                      $jasa = query("SELECT * FROM jasa WHERE jasa.id_jasa = $idJasa");
                    //   $gallery = query("SELECT * FROM products_galleries INNER JOIN products ON products_galleries.product_id = products.id_product WHERE products_galleries.product_id = $idProduct");
                      $banyak += $tj["jumlah"];
                      $total2 = $tj["harga"] * $tj["jumlah"];
                    ?>
                    <tr>
                      <td style="width: 10%;">
                        <img src="../admin/picjasa/<?= $tj["foto_jasa"]; ?>" alt="" class="cart-image w-100">
                      </td>
                      <td style="width: 20%;">
                        <div class=""><?= $tj["nama_jasa"]; ?></div>
                        <!-- <div class="product-title"><?= $t["unit"] / 1000; ?> Kilogram</div> -->
                      </td>
                      <td style="width: 20%;">
                        <div class="">#<?= $tj["kode_produk"]; ?></div>
                      </td>
                      <td style="width: 10%;">
                        <div class=""><?= $tj["jumlah"]; ?></div>
                      </td>
                      <td style="width: 20%;">
                        <div class="">Rp. <?= number_format($tj["harga"]); ?></div>
                        <div class="product-title">IDR</div>
                      </td>
                      <td style="width: 20%;">
                        <div class="">Rp. <?= number_format($total2); ?></div>
                        <div class="product-title">IDR</div>
                      </td>
                    </tr>
                  <?php endforeach; } ?>
                </tbody>
              </table>
            </div>
        </div>
        <form action="" method="POST">
          <div class="row" data-aos="fade-up" data-aos-delay="150">
            <div class="col-12">
              <hr>
            </div>
            <div class="col-12">
              <h2>
                Shipping Details
              </h2>
            </div>
          </div>
          <div class="row mb-2" data-aos="fade-up" data-aos-delay="200">
            
            <div class="col-md-12">
              <div class="form-group">
                <label for="no_hp">No HP / WA</label>
                <input type="tel" readonly name="no_hp" id="no_hp" class="form-control form-store" value="<?= $user["no_hp"] ?? ''; ?>" required>
              </div>
            </div>
            <div class="col-md-12" id="alamat">
              <div class="form-group">
                <label for="alamat">Alamat</label>
                <textarea name="alamat" readonly id="editor"><?= $user["alamat"] ?? ''; ?></textarea>
              </div>
            </div>
            <?php 
          $id_pembayaran = $transactionMain["id_pembayaran"];
          $rekening = query("SELECT * FROM pembayaran WHERE id_pembayaran = $id_pembayaran")[0];
          ?>
            <div class="col-md-12">
              <div class="form-group">Rekening Pembayaran</div>
              <input type="text" class="form-control" readonly name="id_pembayaran" value="<?= $rekening['nama_pembayaran'].' - '.$rekening['atas_nama']; ?>">
            </div>
          </div>
          
          <div class="row">
          <div class="col-6">
              Foto Penjemputan <br>
              <?php if($transactionMain['foto_jemput'] == '') : ?>
                <p class="text-muted">Belum ada foto</p>
                <?php else : ?>
                  <img src="../admin/foto_jemput/<?= $transactionMain["foto_jemput"]; ?>" class="w-50" alt="">
                <?php endif; ?>
            </div>
            <div class="col-6">
              Foto Penerimaan <br>
              <?php if($transactionMain['foto_penerimaan'] == '') : ?>
                <p class="text-muted">Belum ada foto</p>
                <?php else : ?>
                  <img src="../admin/foto_penerimaan/<?= $transactionMain["foto_penerimaan"]; ?>" class="w-50" alt="">
                <?php endif; ?>
            </div>
          </div>

          <div class="row" data-aos="fade-up" data-aos-delay="150">
            <div class="col-12">
              <h2 class="mb-2">
                Informasi Transaksi
              </h2>
            </div>
          </div>
          
          <div class="row justify-content-center mb-5" data-aos="fade-up" data-aos-delay="200">
            <div class="col-4 col-md-3">
              <div class="font-weight-bold"><?= $banyak; ?></div>
              <div class="text-muted">Banyak Barang</div>
            </div>
            <div class="col-4 col-md-3">
              <!-- <div class="font-weight-bold"><?= $transactionMain["weight_total"] / 1000; ?> Kilogram</div>
              <div class="text-muted">Berat Barang</div> -->
            </div>
            <div class="col-4 col-md-3">
              <?php if ($transactionMain["status"] == 'BELUM KONFIRMASI') : ?>
                <div class="text-danger font-weight-bold"><?= ($transactionMain["status"]); ?></div>
              <?php else : ?>
                <div class="text-success font-weight-bold"><?= ($transactionMain["status"]); ?></div>
              <?php endif;?>
              <div class="text-muted">Status Pembayaran</div>
            </div>
            <div class="col-4 col-md-3">
              <div class="text-success font-weight-bold">Rp. <?= number_format($transactionMain["total_harga"]); ?></div>
              <div class="text-muted">Total</div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  



