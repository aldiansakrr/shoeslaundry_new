-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 06, 2023 at 03:12 PM
-- Server version: 5.7.36
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `_shoeslaundry`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksiproduk`
--

DROP TABLE IF EXISTS `detail_transaksiproduk`;
CREATE TABLE IF NOT EXISTS `detail_transaksiproduk` (
  `id_detail_transaksiproduk` int(11) NOT NULL AUTO_INCREMENT,
  `id_transaksiproduk` int(11) NOT NULL,
  `id_produk` int(11) DEFAULT NULL,
  `id_jasa` int(11) DEFAULT NULL,
  `harga` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `kode_produk` varchar(20) NOT NULL,
  PRIMARY KEY (`id_detail_transaksiproduk`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_transaksiproduk`
--

INSERT INTO `detail_transaksiproduk` (`id_detail_transaksiproduk`, `id_transaksiproduk`, `id_produk`, `id_jasa`, `harga`, `jumlah`, `kode_produk`) VALUES
(21, 19, NULL, 17, 145000, 1, 'PRD-25178'),
(24, 21, NULL, 18, 40000, 1, 'PRD-83475'),
(23, 20, NULL, 16, 165000, 1, 'PRD-86579');

-- --------------------------------------------------------

--
-- Table structure for table `jasa`
--

DROP TABLE IF EXISTS `jasa`;
CREATE TABLE IF NOT EXISTS `jasa` (
  `id_jasa` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jasa` varchar(100) NOT NULL,
  `harga_jasa` int(11) NOT NULL,
  `jenis_jasa` varchar(20) NOT NULL,
  `deskripsi_jasa` text NOT NULL,
  `foto_jasa` varchar(255) NOT NULL,
  PRIMARY KEY (`id_jasa`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jasa`
--

INSERT INTO `jasa` (`id_jasa`, `nama_jasa`, `harga_jasa`, `jenis_jasa`, `deskripsi_jasa`, `foto_jasa`) VALUES
(18, 'Fast Clean', 40000, 'Cleaning', '<p>Treatment membersihkan / cuci sepatu secara cepat khusus untuk sepatu ( lebih dikhususkan&nbsp;<em>sneakers / loafers )</em>&nbsp;aman untuk semua bahan dan warna.</p>\r\n\r\n<p>Pengerjaan dapat ditunggu di <em>store</em>.</p>\r\n\r\n<p>Dilakukan secara manual sehingga aman untuk semua jenis sepatu.</p>\r\n\r\n<p>Aman untuk semua bahan material dan warna sepatu karena menggunakan bahan alami dan teknik yang benar.</p>\r\n\r\n<p>Bagian yang di treatment :&nbsp;<strong>upper,midsole</strong></p>\r\n\r\n<p>Durasi pengerjaan +- 45 menit.</p>\r\n', '473-fastclean.png'),
(27, 'Reglue', 65000, 'Others', '<p>Treatment penyatuan kembali bagian sepatu yang lepas dengan cara di lem.</p>\r\n\r\n<p>Menggunakan lem khusus yang kuat, sehingga lebih tahan lama.</p>\r\n\r\n<p>Untuk hasil lem yang maksimal kami juga menggunakan alat press.</p>\r\n\r\n<p>Bagian yang di treatment :&nbsp;<strong>midsole,outsole</strong></p>\r\n\r\n<p>Durasi pengerjaan +- 2 hari.</p>\r\n', '664-SEWING.png'),
(13, 'Repaint Boost', 125000, 'Repaint', '<p>Treatment pewarnaan khusus untuk&nbsp;<em>boost</em>&nbsp;sepatu, aman untuk bahan&nbsp;<em>boost</em>. Dilakukan untuk mengembalikan warna boost yang&nbsp;<em>menguning.</em></p>\r\n\r\n<p>Dilakukan secara manual dan detail sehingga aman untuk sepatu.</p>\r\n\r\n<p>Aman untuk sepatu karena menggunakan pewarna khusus untuk boost dengan teknik yang benar.</p>\r\n\r\n<p>Bagian yang di treatment :&nbsp;<strong>midsole boost</strong></p>\r\n\r\n<p>Durasi pengerjaan +- 5 hari</p>\r\n\r\n<p>Warna dapat request sesuai keinginan dengan menanyakan langsung kepada tim kami.&nbsp;</p>\r\n', '520-repaint-boost.png'),
(14, 'Repaint Canvas', 100000, 'Repaint', '<p>Treatment pewarnaan khusus untuk sepatu berbahan canvas untuk mencerahkan warna yang pudar.</p>\r\n\r\n<p>Aman untuk sepatu karena menggunakan pewarna khusus untuk sepatu dan teknik yang benar.</p>\r\n\r\n<p>Sepatu tetap lentur karena menggunakan pewarna khusus.</p>\r\n\r\n<p>Treatment ini sudah mendapatkan&nbsp;<strong>Free DeepClean.</strong></p>\r\n\r\n<p>Dilakukan secara manual dan detail sehingga aman untuk sepatu.</p>\r\n\r\n<p>Bagian yang di treatment :&nbsp;<strong>upper canvas</strong></p>\r\n\r\n<p>Durasi pengerjaan +- 5 hari.</p>\r\n\r\n<p>Warna dapat&nbsp;<em>request</em>&nbsp;sesuai keinginan dengan menanyakan langsung pada tim kami.</p>\r\n', '941-repaint-canvas.png'),
(15, 'Repaint Leather', 185000, 'Repaint', '<p>Treatment pewarnaan khusus untuk sepatu berbahan leather / kulit, solusi untuk mencerahkan warna yang pudar atau mulai kusam.</p>\r\n\r\n<p>Aman untuk sepatu karena menggunakan pewarna khusus untuk sepatu dan teknik yang benar.</p>\r\n\r\n<p>Sepatu tetap lentur tidak kaku karena menggunakan pewarna khusus.</p>\r\n\r\n<p>Treatment pewarnaan sepatu berbahan kulit ini sudah mendapatkan&nbsp;<strong>Free DeepClean.</strong></p>\r\n\r\n<p>Dilakukan secara manual dan detail sehingga aman untuk sepatu.</p>\r\n\r\n<p>Bagian yang di treatment :&nbsp;<strong>upper leather / kulit</strong></p>\r\n\r\n<p>Durasi pengerjaan +- 5 hari.</p>\r\n\r\n<p>Warna dapat&nbsp;<em>request</em>&nbsp;sesuai keinginan dengan menanyakan langsung pada tim kami.</p>\r\n', '889-repaint-leather.png'),
(16, 'Repaint Suede', 165000, 'Repaint', '<p>Treatment pewarnaan khusus untuk sepatu berbahan suede, solusi untuk mencerahkan warna yang pudar atau mulai kusam.</p>\r\n\r\n<p>Aman untuk sepatu karena menggunakan pewarna khusus untuk sepatu dan teknik yang benar.</p>\r\n\r\n<p>Sepatu tetap lentur tidak kaku karena menggunakan pewarna khusus.</p>\r\n\r\n<p>Treatment ini sudah mendapatkan&nbsp;<strong>Free DeepClean.</strong></p>\r\n\r\n<p>Dilakukan secara manual dan detail sehingga aman untuk sepatu.</p>\r\n\r\n<p>Bagian yang di treatment :&nbsp;<strong>upper suede</strong></p>\r\n\r\n<p>Durasi pengerjaan +- 5 hari.</p>\r\n\r\n<p>Warna dapat&nbsp;<em>request</em>&nbsp;sesuai keinginan dengan menanyakan langsung pada tim kami.</p>\r\n', '681-repaint-SUEDE.png'),
(17, 'Repaint Midsole', 145000, 'Repaint', '<p>Treatment pewarnaan khusus untuk&nbsp;<em>midsole</em>&nbsp;sepatu, aman untuk bahan rubber/karet. Solusi lain untuk mengembalikan warna midsole yang&nbsp;<em>menguning&nbsp;</em>selain treatment Unyellowing.</p>\r\n\r\n<p>Treatment ini sudah mendapatkan&nbsp;<strong>Free DeepClean.</strong></p>\r\n\r\n<p>Dilakukan secara manual dan detail sehingga aman untuk sepatu.</p>\r\n\r\n<p>Aman untuk sepatu karena menggunakan pewarna khusus untuk midsole dan teknik yang benar.</p>\r\n\r\n<p>Bagian yang di treatment :&nbsp;<strong>midsole rubber/foam/wood</strong></p>\r\n\r\n<p>Durasi pengerjaan +- 5 hari.</p>\r\n\r\n<p>Warna dapat&nbsp;<em>request</em>&nbsp;sesuai keinginan dengan menanyakan langsung pada tim kami.</p>\r\n', '299-repaint-midsole.png'),
(19, 'Deep Clean', 125000, 'Cleaning', '<p>Treatment pencucian khusus untuk sepatu secara detail untuk seluruh bagian,&nbsp;aman untuk semua bahan dan warna.</p>\r\n\r\n<p>Dilakukan secara manual sehingga aman untuk semua jenis sepatu</p>\r\n\r\n<p>Aman untuk semua bahan material dan warna sepatu karena menggunakan bahan alami dan teknik yang benar.</p>\r\n\r\n<p>Bagian yang di treatment :&nbsp;<strong>upper,midsole,outsole,insole,laces</strong></p>\r\n\r\n<p>Durasi pengerjaan +- 3 hari.</p>\r\n', '141-Deepclean.png'),
(20, 'Deep Clean Express', 85000, 'Cleaning', '<p>Treatment pencucian khusus untuk sepatu secara detail untuk seluruh bagian,&nbsp;aman untuk semua bahan dan warna.</p>\r\n\r\n<p>Dilakukan secara manual sehingga aman untuk semua jenis sepatu.</p>\r\n\r\n<p>Aman untuk semua bahan material dan warna sepatu karena menggunakan bahan alami dan teknik yang benar.</p>\r\n\r\n<p>Bagian yang di treatment :&nbsp;<strong>upper,midsole,outsole,insole,laces</strong></p>\r\n\r\n<p>Durasi pengerjaan&nbsp;<strong>24</strong>&nbsp;jam.</p>\r\n', '685-Deepclean-express-1.png'),
(21, 'Deep Clean ( Whitening )', 115000, 'Cleaning', '<p>Treatment pencucian khusus untuk sepatu berbahan&nbsp;<strong>canvas</strong>&nbsp;dan&nbsp;<strong>mesh&nbsp;</strong>berwarna putih.</p>\r\n\r\n<p>Baik untuk membersihkan noda hitam atau menghilangkan noda menguning pada upper sepatu berwarna putih ataupun warna terang.</p>\r\n\r\n<p>Dilakukan secara manual sehingga aman untuk semua jenis sepatu</p>\r\n\r\n<p>Bagian yang di treatment :&nbsp;<strong>upper</strong></p>\r\n\r\n<p>Durasi pengerjaan +- 2 hari.</p>\r\n', '29-Deepcleanwhitening.png'),
(22, 'Deep Clean Kid Shoes', 65000, 'Cleaning', '<p>Treatment pencucian khusus untuk sepatu anak-anak aman untuk semua bahan dan warna.</p>\r\n\r\n<p>Dilakukan secara manual sehingga aman untuk semua jenis sepatu</p>\r\n\r\n<p>Aman untuk kulit bayi karena bahan pembersih tidak menggandung bahan kimia berbahaya</p>\r\n\r\n<p>Aman untuk semua bahan material dan warna sepatu karena menggunakan bahan alami dan teknik yang benar.</p>\r\n\r\n<p>Bagian yang di treatment :&nbsp;<strong>insole,</strong><strong>upper,outsole,midsole,laces</strong></p>\r\n\r\n<p>Durasi pengerjaan +- 2 hari.</p>\r\n', '950-Deepclean-kid-shoes.png'),
(23, 'Deep Clean Women Shoes', 142500, 'Cleaning', '<p>Treatment pencucian khusus untuk sepatu wanita (&nbsp;<em>Heels, Wedges, Flatshoes&nbsp;</em>)&nbsp;aman untuk semua bahan dan warna.</p>\r\n\r\n<p>Dilakukan secara manual sehingga aman untuk semua jenis sepatu.</p>\r\n\r\n<p>Aman untuk semua bahan material dan warna sepatu karena menggunakan bahan alami dan teknik yang benar.</p>\r\n\r\n<p>Bagian yang di treatment :&nbsp;<strong>insole,</strong><strong>upper,outsole</strong></p>\r\n\r\n<p>Durasi pengerjaan +- 3 hari.</p>\r\n', '924-Deepclean-women-shoes.png'),
(24, 'Unyellowing', 95000, 'Others', '<p>Treatment layanan khusus untuk&nbsp;<em>midsole</em>&nbsp;sepatu yang yellowing, aman untuk bahan rubber/karet. Solusi untuk mengembalikan warna midsole yang&nbsp;<em>menguning&nbsp;</em>selain treatment <strong>Repaint Midsole</strong>.</p>\r\n\r\n<p>Menggunakan bahan kimia khusus untuk mengoksidasi midsole sepatu.</p>\r\n\r\n<p>Perlakuan treatment hanya dilakukan maksimal 2x.</p>\r\n\r\n<p>Bagian yang di treatment :&nbsp;<strong>midsole rubber(karet)</strong></p>\r\n\r\n<p>Durasi pengerjaan +- 2 hari.</p>\r\n', '676-Unyellowing.png'),
(25, 'Sewing', 100000, 'Others', '<p>Treatment penyatuan kembali bagian sepatu yang lepas dengan cara di jahit.</p>\r\n\r\n<p>Menggunakan benang khusus yang kuat, sehingga lebih tahan lama.</p>\r\n\r\n<p>Hasil jahitan rapi, sehingga sepatu tetap nyaman dan bagus saat di pandang.</p>\r\n\r\n<p>Bagian yang di treatment :&nbsp;<strong>midsole,outsole</strong></p>\r\n\r\n<p>Durasi pengerjaan +- 1 hari.</p>\r\n', '519-SEWING.png'),
(26, 'Nano Protect', 152500, 'Others', '<p>Treatment pelapisan bagian upper sepatu agar kedap cairan.</p>\r\n\r\n<p>Aman untuk semua bahan material dan warna sepatu karena menggunakan bahan alami dan teknik yang benar.</p>\r\n\r\n<p>Sifat melapisi bahan dari cairan saja, sehingga sepatu/tas tetap dapat sirkulasi udara.</p>\r\n\r\n<p>Bagian yang di treatment :&nbsp;<strong>upper</strong></p>\r\n\r\n<p>Durasi pengerjaan +- 2 hari.</p>\r\n', '94-Nanoprotect.png');

-- --------------------------------------------------------

--
-- Table structure for table `keranjang`
--

DROP TABLE IF EXISTS `keranjang`;
CREATE TABLE IF NOT EXISTS `keranjang` (
  `id_keranjang` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_produk` int(11) DEFAULT NULL,
  `id_jasa` int(11) DEFAULT NULL,
  `jumlah` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  PRIMARY KEY (`id_keranjang`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keranjang`
--

INSERT INTO `keranjang` (`id_keranjang`, `id_user`, `id_produk`, `id_jasa`, `jumlah`, `total`) VALUES
(50, 15, NULL, 18, 2, 80000),
(49, 15, NULL, 18, 2, 80000);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

DROP TABLE IF EXISTS `pegawai`;
CREATE TABLE IF NOT EXISTS `pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(100) NOT NULL,
  `email_pegawai` varchar(100) NOT NULL,
  `password_pegawai` varchar(255) NOT NULL,
  `telp_pegawai` varchar(20) NOT NULL,
  `no_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `email_pegawai`, `password_pegawai`, `telp_pegawai`, `no_pegawai`) VALUES
(2, 'Fransiskus Renaldi Tiwu', 'fransiskus.tiwu@gmail.com', '$2y$10$Zcc0wDeDlY9nb11GPXy4YupheFvXJ/vaKlMJXGtgXMO7JmQTaucvK', '089898', 222113);

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

DROP TABLE IF EXISTS `pembayaran`;
CREATE TABLE IF NOT EXISTS `pembayaran` (
  `id_pembayaran` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pembayaran` varchar(100) NOT NULL,
  `nomor` varchar(20) NOT NULL,
  `atas_nama` varchar(100) NOT NULL,
  PRIMARY KEY (`id_pembayaran`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`id_pembayaran`, `nama_pembayaran`, `nomor`, `atas_nama`) VALUES
(1, 'OVO', '8777777', 'Aldiansa Kautsar Ramadhani'),
(3, 'BANK BCA', '1231232', 'Muhammad Andi Wibowo'),
(4, 'BANK BNI', '178688999', 'Mochammad Andi Rahmansyah');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

DROP TABLE IF EXISTS `produk`;
CREATE TABLE IF NOT EXISTS `produk` (
  `id_produk` int(11) NOT NULL AUTO_INCREMENT,
  `nama_produk` varchar(100) NOT NULL,
  `harga_produk` int(11) NOT NULL,
  `deskripsi_produk` text NOT NULL,
  `stok_produk` int(11) NOT NULL,
  `foto_produk` varchar(255) NOT NULL,
  PRIMARY KEY (`id_produk`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `nama_produk`, `harga_produk`, `deskripsi_produk`, `stok_produk`, `foto_produk`) VALUES
(3, 'Paket Shoe Cleaner', 89000, '<ul>\r\n	<li>Paket Shoe Cleaner</li>\r\n</ul>\r\n\r\n<p>Dalam Paket Shoe Cleaner terdapat :</p>\r\n\r\n<ol>\r\n	<li>Shoe Cleaner 100ml</li>\r\n	<li>Lap Microfiber</li>\r\n	<li>Standard Brush</li>\r\n</ol>\r\n\r\n<p>Shoe Cleaner&nbsp; adalah sabun pencuci sepatu dengan bahan yang ramah lingkungan juga aman dan sangat efektif dalam membersihkan noda dan kotoran pada sepatu bisa juga di pakai pada tas, topi atau helm anda. Produk ini juga mengandung bahan - bahan yang dapat membunuh bakteri dan jamur sehingga sepatu anda terhindar dari bau tak sedap. Produk ini sangat aman digunakan karena tidak mengandung detergent.</p>\r\n\r\n<p>Cara pakai :</p>\r\n\r\n<ol>\r\n	<li>Siapkan air di dalam wadah secukupnya</li>\r\n	<li>Celupkan sikat dalam air lalu beri cleaner secukupnya ke permukaan sikat</li>\r\n	<li>Celupkan lagi sikat dalam mangkuk air</li>\r\n	<li>Sikat sepatu</li>\r\n	<li>Lap dengan microfiber</li>\r\n	<li>Cukup keringkan depan kipas angin (instant)</li>\r\n	<li>Lakukan lebih dari 1x untuk penggunaan yang lebih maximal</li>\r\n</ol>\r\n', 85, '875-produk2.png'),
(2, 'Paket Clean Shoes 1', 99900, '<p>Dalam Paket Clean Shoes 1 terdapat :</p>\r\n\r\n<p>1. Foam Clean Shoes 100ml</p>\r\n\r\n<p>2. Lap Microfiber</p>\r\n\r\n<p>3. Premiun Brush</p>\r\n\r\n<p>Foam Clean Shoes 100ml solusi terbaik ketika anda ingin membersihkan sepatu dari kotoran dan noda membandel saat anda tidak ingin membuang lebih banyak waktu ketika proses pembersihannya. Sangat cocok ketika anda sedang terburu buru dan dikejar deadline. Bisa di bawa&nbsp;kemana aja dalam&nbsp;kondisi apapun karena Foam Clean Shoes tdk perlu di bilas saat pembersihan sepatu yang kotor dan terkena noda, cukup di lap saja sudah bersih. Foam Clean Shoes hanya untuk membersihkan bukan untuk mengembalikan warna yg sudah pudar, atau noda yg sudah meresap ke bahan sepatu sehingga merubah warna sepatu, kalau ingin mengembalikan warna sepatu seperti semula solusinya di repaint saja yah.</p>\r\n', 86, '176-produk1.png'),
(4, 'Premium Shoe Cleaner', 225000, '<p>Bisa digunakan untuk semua warna dan tidak membuat warna sepatu memudar karena shoes cleaner kami terbuat dari Natural oil non detergent yang sudah terbukti aman dan Sangat efektif untuk mencuci sepatu.</p>\r\n\r\n<p>Premium Shoe Cleaner bisa digunakan untuk segala jenis (All material) sepatu seperti : suede, nubuck, leather, canvas dll. Premium Shoe Cleaner berfungsi untuk membersihkan kotoran dan noda pada sepatu. Dapat digunakan sampai dengan 40 pasang sepatu untuk 1 botol 500 ml nya.</p>\r\n\r\n<p>Cara Penggunaan :</p>\r\n\r\n<p>1. Basahi terlebih dahulu sikat pembersih dianjurkan memakai Sikat sepatu standar.</p>\r\n\r\n<p>2. Tuangkan Ccleaner.id Shoe Cleaner pada sikat pembers yang Sudah basah.</p>\r\n\r\n<p>3. Sikat sepatu dengan lembut</p>\r\n\r\n<p>4. Lap bersih sepatu menggunakan kain</p>\r\n\r\n<p>5. Ulangi dari langkah pertama apabila diperlukan sampai hasil maksimal</p>\r\n\r\n<p>6. Keringkan / anginkan sepatu agar tidak lembab.</p>\r\n\r\n<p>&nbsp;Anjuran :</p>\r\n\r\n<p>- Kocok terlebih dahulu sebelum produk dituangkan</p>\r\n\r\n<p>- Gunakan Hardbrush untuk midsole dan outsole, dan Softbrush untuk upper.</p>\r\n', 86, '637-produk3.png');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_produk`
--

DROP TABLE IF EXISTS `transaksi_produk`;
CREATE TABLE IF NOT EXISTS `transaksi_produk` (
  `id_transaksiproduk` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `total_harga` int(11) NOT NULL,
  `id_pembayaran` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `jemput` int(11) NOT NULL,
  `foto_jemput` varchar(255) NOT NULL,
  `terkirim` int(11) NOT NULL,
  `foto_transaksiproduk` varchar(255) NOT NULL,
  `code` varchar(20) NOT NULL,
  `penerima` varchar(50) NOT NULL,
  `foto_penerimaan` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `arrived_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_transaksiproduk`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_produk`
--

INSERT INTO `transaksi_produk` (`id_transaksiproduk`, `id_user`, `total_harga`, `id_pembayaran`, `status`, `jemput`, `foto_jemput`, `terkirim`, `foto_transaksiproduk`, `code`, `penerima`, `foto_penerimaan`, `created_at`, `arrived_at`) VALUES
(19, 15, 145000, 1, 'TERKIRIM', 1, '63b57963b0fa7.jpg', 1, '63b56e2618eee.jpg', 'EZM-95158', 'Ibu Elvina', '63b57b401b105.jpg', '2023-01-04 12:06:31', NULL),
(21, 15, 40000, 1, 'TERKIRIM', 1, '63b58ab9edd56.jpg', 1, '63b585d138e37.jpg', 'EZM-30807', 'Mariati Finda Ovalina', '63b590465d9fa.jpg', '2023-01-04 13:56:50', '2023-01-04 14:42:10'),
(20, 14, 165000, 3, 'BELUM KONFIRMASI', 1, '', 1, '', 'EZM-60879', '', '', '2023-01-04 13:54:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `alamat` text NOT NULL,
  `role` varchar(20) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama`, `email`, `password`, `no_hp`, `alamat`, `role`) VALUES
(7, 'Aldiansa Kautsar Ramadhani', 'bajevskia@gmail.com', '$2y$10$Gykzd4SbzqPMvQAaf/2.g./MVMHW.l8quxl2rQGaYivI/wAyryAc2', '', '', 'ADMIN'),
(10, 'Annisya Rahmadhanti', 'annisarahmadhanti@gmail.com', '$2y$10$tEmtt/k1EkZzkmy.Uv9pT.3RQil3sZyLzGMj433ZYBZj/A9ydtFUK', '', '', 'USER'),
(9, 'Mochamad Andi Rahmansyah', 'andirahmansyah@gmail.com', '$2y$10$Ip5yO7jd4VlZP5FRAz3Su.H.Ikx6BE2fml0M7SfZRWHP3ptk9Kkoy', '', '', 'ADMIN'),
(8, 'Muhammad Andi Wibowo', 'andiwibowo@gmail.com', '$2y$10$DiO8HvSVruAMwMmh16x/GOX3lHhsdYIq20MSqavfNEdtbzyFWVjCm', '', '', 'ADMIN'),
(11, 'Salsabila Kharisma', 'salsabilla@gmail.com', '$2y$10$48khssAQRc85McIFCNMXheFzc7/zTC0OHctaXQGWQNmvWUeZLlD6O', '', '', 'USER'),
(12, 'Risky Wahyu Nur Hamzah', 'riskyhamzah@gmail.com', '$2y$10$uGN1hg82zec5gysDVArfm.Td8Z9pd.3fs9X8Rnk7jBUR5PeXru2c2', '0828282', 'Kediri', 'USER'),
(14, 'Yohana Archangela', 'cindiundi@gmail.com', '$2y$10$31MurqcU/zsY0TpTPSLKL.fiivaOMUO30/DurhU4/cgKlnMGfszBa', '0897676722', '<p>Klayatan 2 Gg Melati</p>\r\n', 'USER'),
(15, 'Elvina Rosanti', 'vinarosant@gmail.com', '$2y$10$oi3KaB.LoqmI41FL1Kwu7egcgXHY652d/qcVLVYsJjQ.1XnkdcOh.', '0897676722', '<p>Binangun Baru Kecamatan Pakisaji</p>\r\n', 'USER'),
(16, 'Ahmad Mahbub Junaidi', 'ahmadmahbub@gmail.com', '$2y$10$.a59pwt0nzTVzwZ8eLgwbOwNkxZvzHv6zYE.onT20SQG66w9EQNuq', '087676', 'Klayatan', 'USER'),
(17, 'Ramadhani', 'rama@gmail.com', '$2y$10$J.wtsJ9f54I/QB19WCRnd.nuBwzq5Y6v7cahBR.JvwhD3xd3M66ki', '09867974', '<p>jl keben</p>\r\n', 'USER');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
