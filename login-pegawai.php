<?php 
require_once 'config/config.php';

if (isset($_SESSION["user"])) {
  $id = $_SESSION["user"];
  $result = query("SELECT * FROM user WHERE id_user = $id")[0];
  if ($result['role'] == 'ADMIN') {
    header("Location: admin");
  } elseif($result["role"] == 'OWNER') {
    header("Location: owner");
  }
}

if (isset($_SESSION["login"]) && isset($_SESSION["pegawai"])) {
  header("Location: pegawai/index.php");
}

if (isset($_POST["login"])) {
  $email_pegawai = $_POST["email_pegawai"];
  $password_pegawai = $_POST["password_pegawai"];

  $result = mysqli_query($conn, "SELECT * FROM pegawai WHERE email_pegawai = '$email_pegawai'");
  if (mysqli_num_rows($result) === 1) {
    
    $row = mysqli_fetch_assoc($result);
    if (password_verify($password_pegawai, $row["password_pegawai"])) {
      $_SESSION["login"] = true;
      $_SESSION["pegawai"] = $row["id_pegawai"];
      header("Location: pegawai/index.php");
    }
  }

  $error = true;

}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>Login Pegawai | Clean Shoes</title>

    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />
    <link href="assets/style/main.css" rel="stylesheet" />
    <link rel="icon" type="image/png" href="gambar/logo.png">
  </head>

  <body>
    <!-- navbar -->
    <nav
      class="navbar navbar-expand-lg navbar-light navbar-store fixed-top navbar-fixed-top"
      data-aos="fade-down"
    >
      <div class="container">
        <a href="index.php" class="navbar-brand" title="home">
          <img src="gambar/logo.png" class="w-50" alt="logo" />
        </a>
        <button
          class="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarResponsive"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a href="index.php" class="nav-link">Home</a>
            </li>
            <li class="nav-item">
              <a href="products.php" class="nav-link">All Products</a>
            </li>
            <li class="nav-item">
            <a href="jasas.php" class="nav-link">All Jasas</a>
          </li>
            <li class="nav-item">
              <a href="about.php" class="nav-link">About</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- akhir navbar -->

    <!-- page content -->
    <div class="page-content page-auth">
      <section class="store-auth" data-aos="fade-up">
        <div class="container">
          <div class="row align-items-center row-login">
            <div class="col-lg-6 text-center">
              <img
                src="./gambar/6343825.jpg"
                alt=""
                class="w-100 mb-lg-none"
              />
            </div>
            <div class="col-lg-5">
              <h2 class="mb-4">Yuk antar pesanan pelanggan, Login pegawai</h2>
              <form action="" method="POST" class="mt-3">
                  <?php if (isset($error)) : ?>
                      <div class="alert alert-danger w-75">
                          <p class="font-weight-bold m-0">Maaf, Email / Username salah</p>
                      </div>
                  <?php endif;?>
                <div class="form-group">
                  <label for="email_pegawai">Email Address</label>
                  <input
                    type="email"
                    name="email_pegawai"
                    id="email"
                    class="form-control w-75"
                  />
                </div>
                <div class="form-group">
                  <label for="password_pegawai">Password</label>
                  <input
                    type="password"
                    name="password_pegawai"
                    id="password"
                    class="form-control w-75"
                  />
                </div>
                <button
                  type="submit"
                  name="login"
                  class="btn btn-success btn-block w-75 mt-4"
                  >Sign In to My Account</button>
                <a
                  href="index.php"
                  class="btn btn-login btn-block w-75 mt-2"
                  >Back To Home</a
                >
              </form>
            </div>
          </div>
        </div>
      </section>
    </div>

    <!-- footer -->
  <?php include "footer.php"; ?>
    <!-- akhir footer -->

    <!-- Bootstrap core JavaScript -->
    <script src="assets/vendor/jquery/jquery.slim.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
      AOS.init();
    </script>
    <script src="assets/js/navbar-scroll.js"></script>
  </body>
</html>
