<?php 
$id_transaksiproduk = $_GET["id_transaksiproduk"];
$produk_transaction_details = query("SELECT * FROM detail_transaksiproduk 
INNER JOIN transaksi_produk ON detail_transaksiproduk.id_transaksiproduk = 
transaksi_produk.id_transaksiproduk INNER JOIN produk ON detail_transaksiproduk.id_produk = 
produk.id_produk WHERE detail_transaksiproduk.id_transaksiproduk = $id_transaksiproduk");
$jasa_transaction_details = query("SELECT * FROM detail_transaksiproduk 
INNER JOIN transaksi_produk ON detail_transaksiproduk.id_transaksiproduk = 
transaksi_produk.id_transaksiproduk INNER JOIN jasa ON detail_transaksiproduk.id_jasa = 
jasa.id_jasa WHERE detail_transaksiproduk.id_transaksiproduk = $id_transaksiproduk");

?>
  <nav
  class="navbar navbar-expand-lg navbar-light navbar-store fixed-top"
  data-aos="fade-down"
>
  <div class="container-fluid">
    <button
      class="btn btn-secondary d-md-none mr-auto mr-2"
      id="menu-toggle"
    >
      &laquo; Menu
    </button>
    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarResponsive"
    >
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collpase navbar-collapse" id="navbarResponsive">
      <!-- dekstop menu -->
      <ul class="navbar-nav d-none d-lg-flex ml-auto">
        <li class="nav-item dropdown">
          <a
            href="#"
            class="nav-link"
            id="navbarDropdown"
            role="button"
            data-toggle="dropdown"
          >
          <img
              src="../assets/images/person-circle.svg"
              alt="profile"
              height="40px"
              class="rounded-circle mr-2 profile-picture"
            />
            <?php 
              $id_pegawai = $_SESSION['pegawai'];
              $pegawai = query("SELECT * FROM pegawai WHERE id_pegawai = $id_pegawai")[0];
            ?>
            Hi, <?= $pegawai["nama_pegawai"]; ?>
          </a>
          <div class="dropdown-menu">
            <a href="../../logout.php" class="dropdown-item">logout</a>
          </div>
        </li>
      </ul>

      <!-- mobile app -->
      <ul class="navbar-nav d-block d-lg-none">
        <li class="nav-item">
          <a href="" class="nav-link"> Hi, <?= $pegawai["nama_pegawai"]; ?></a>
        </li>
      </ul>
    </div>
  </div>
</nav>
  <div class="section-content section-dashboard-home" data-aos="fade-up">
    <div class="container-fluid">
      <div class="dashboard-heading">
      <?php 
        $transactionMain = query("SELECT * FROM transaksi_produk INNER JOIN user ON transaksi_produk.id_user = user.id_user WHERE id_transaksiproduk = $id_transaksiproduk")[0];
      ?>
        <h2 class="dashboard-title"><?= $transactionMain["nama"]; ?></h2>
        <p class="dashboard-subtitle">#<?= $transactionMain["code"]; ?></p>
      </div>
      <div class="dashboard-content store-cart">
        <div class="row" data-aos="fade-up" data-aos-delay="100">
            <div class="col-12 table-responsive">
              <table class="table table-borderless table-cart">
                <thead>
                  <tr>
                    <th>Image</th>
                    <th>Name &amp; Seller</th>
                    <th>Kode Produk</th>
                    <th>Banyak</th>
                    <th>Price</th>
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody>
                <?php 
                $jumlah = 0;
                $total = 0;
                ?>
                  <?php if(isset($produk_transaction_details)){ foreach ($produk_transaction_details as $t) : ?>
                    <?php 
                      $idProduct = $t["id_produk"];
                      $produk = query("SELECT * FROM produk WHERE produk.id_produk = $idProduct");
                      // $gallery = query("SELECT * FROM products_galleries INNER JOIN products ON products_galleries.product_id = products.id_product WHERE products_galleries.product_id = $idProduct");
                      $jumlah += $t["jumlah"];
                      $total = $t["harga"] * $t["jumlah"];
                    ?>
                    <tr>
                      <td style="width: 10%;">
                        <img src="../admin/produk<?= $produk["foto_produk"]; ?>" alt="" class="cart-image w-100">
                      </td>
                      <td style="width: 20%;">
                        <div class=""><?= $t["nama_produk"]; ?></div>
                        <!-- <div class="product-title"><?= $t["unit"] / 1000; ?> Kilogram</div> -->
                      </td>
                      <td style="width: 20%;">
                        <div class="">#<?= $t["kode_produk"]; ?></div>
                      </td>
                      <td style="width: 10%;">
                        <div class=""><?= $t["jumlah"]; ?></div>
                      </td>
                      <td style="width: 20%;">
                        <div class="">Rp. <?= number_format($t["harga"]); ?></div>
                        <div class="product-title">IDR</div>
                      </td>
                      <td style="width: 20%;">
                        <div class="">Rp. <?= number_format($total); ?></div>
                        <div class="product-title">IDR</div>
                      </td>
                    </tr>
                  <?php endforeach; } ?>

                  <?php if(isset($jasa_transaction_details)){ foreach ($jasa_transaction_details as $j) : ?>
                    <?php 
                      $idJasa = $j["id_jasa"];
                      $jasa = query("SELECT * FROM jasa WHERE jasa.id_jasa = $idJasa");
                      // $gallery = query("SELECT * FROM products_galleries INNER JOIN products ON products_galleries.product_id = products.id_product WHERE products_galleries.product_id = $idProduct");
                      $jumlah += $j["jumlah"];
                      $total = $j["harga"] * $j["jumlah"];
                    ?>
                    <tr>
                      <td style="width: 10%;">
                        <img src="../admin/picjasa/<?= $j["foto_jasa"]; ?>" alt="" class="cart-image w-100">
                      </td>
                      <td style="width: 20%;">
                        <div class=""><?= $j["nama_jasa"]; ?></div>
                        <!-- <div class="product-title"><?= $t["unit"] / 1000; ?> Kilogram</div> -->
                      </td>
                      <td style="width: 20%;">
                        <div class="">#<?= $j["kode_produk"]; ?></div>
                      </td>
                      <td style="width: 10%;">
                        <div class=""><?= $j["jumlah"]; ?></div>
                      </td>
                      <td style="width: 20%;">
                        <div class="">Rp. <?= number_format($j["harga"]); ?></div>
                        <div class="product-title">IDR</div>
                      </td>
                      <td style="width: 20%;">
                        <div class="">Rp. <?= number_format($total); ?></div>
                        <div class="product-title">IDR</div>
                      </td>
                    </tr>
                  <?php endforeach; } ?>

                </tbody>
              </table>
            </div>
        </div>
        <form action="" method="POST">
          <div class="row" data-aos="fade-up" data-aos-delay="150">
            <div class="col-12">
              <hr>
            </div>
            <div class="col-12">
              <h2>
                Shipping Details
              </h2>
            </div>
          </div>
          <div class="row mb-2" data-aos="fade-up" data-aos-delay="200">
            <!-- <div class="col-md-4">
              <div class="form-group">
                <label for="city">Kota</label>
                <input type="text" readonly value="<?= $transactionMain["city"]; ?>" class="form-control">
              </div>
            </div> -->
            <div class="col-md-12">
              <div class="form-group">
                <label for="no_hp">No HP / WA</label>
                <input type="tel" readonly name="no_hp" id="no_hp" class="form-control form-store" value="<?= $transactionMain["no_hp"] ?? ''; ?>" required>
              </div>
            </div>
            <!-- <div class="col-md-4">
              <div class="form-group">
                <label for="zip_code">Kode Pos</label>
                <input type="number" readonly name="zip_code" id="zip_code" required value="<?= $transactionMain["postal_code"] ?? ''; ?>" class="form-control form-store">
              </div>
            </div> -->
            <div class="col-md-12" id="alamat">
              <div class="form-group">
                <label for="alamat">Alamat</label>
                <textarea name="alamat" readonly id="editor"><?= $transactionMain["alamat"] ?? ''; ?></textarea>
              </div>
            </div>
          </div>
          <div class="row" data-aos="fade-up" data-aos-delay="150">
            <div class="col-12">
              <hr>
            </div>
            <div class="col-12">
              <h2 class="mb-2">
                Information product
              </h2>
            </div>
          </div>
          <?php 
          $id_pembayaran = $transactionMain["id_pembayaran"];
          $pembayaran = query("SELECT * FROM pembayaran WHERE id_pembayaran = $id_pembayaran")[0];
          ?>
          <div class="row justify-content-center mb-5" data-aos="fade-up" data-aos-delay="200">
            <div class="col-4 col-md-3">
              <div class="font-weight-bold"><?= $jumlah; ?></div>
              <div class="text-muted">Banyak Barang</div>
            </div>
            <div class="col-4 col-md-3">
              <!-- <div class="font-weight-bold"><?= $transactionMain["weight_total"] / 1000; ?> Kilogram</div>
              <div class="text-muted">Berat Barang</div> -->
            </div>
            <div class="col-4 col-md-3">
              <?php if ($transactionMain["status"] == 'BELUM KONFIRMASI') : ?>
                <div class="text-danger font-weight-bold"><?= ($transactionMain["status"]); ?></div>
              <?php else : ?>
                <div class="text-success font-weight-bold"><?= ($transactionMain["status"]); ?></div>
              <?php endif;?>
              <div class="text-muted">Status Pembayaran</div>
            </div>
            <div class="col-4 col-md-3">
              <div class="text-success font-weight-bold">Rp. <?= number_format($transactionMain["total_harga"]); ?></div>
              <div class="text-muted">Total</div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  



