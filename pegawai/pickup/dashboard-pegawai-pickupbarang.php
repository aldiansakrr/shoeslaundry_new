<?php 

$idTransaction = $_GET["id_transaksiproduk"];
$transaction_details = query("SELECT * FROM detail_transaksiproduk INNER JOIN 
transaksi_produk ON detail_transaksiproduk.id_transaksiproduk = 
transaksi_produk.id_transaksiproduk INNER JOIN produk ON 
detail_transaksiproduk.id_produk = produk.id_produk WHERE 
detail_transaksiproduk.id_transaksiproduk = $idTransaction");

if (isset($_POST["fotojemput"])) {
  if (fotoJemput($_POST) > 0) {
    header("Location: ?page=pickup");
  }
}

?>
  <nav
  class="navbar navbar-expand-lg navbar-light navbar-store fixed-top"
  data-aos="fade-down"
>
  <div class="container-fluid">
    <button
      class="btn btn-secondary d-md-none mr-auto mr-2"
      id="menu-toggle"
    >
      &laquo; Menu
    </button>
    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarResponsive"
    >
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collpase navbar-collapse" id="navbarResponsive">
      <!-- dekstop menu -->
      <ul class="navbar-nav d-none d-lg-flex ml-auto">
        <li class="nav-item dropdown">
          <a
            href="#"
            class="nav-link"
            id="navbarDropdown"
            role="button"
            data-toggle="dropdown"
          >
          <img
              src="../assets/images/person-circle.svg"
              alt="profile"
              height="40px"
              class="rounded-circle mr-2 profile-picture"
            />
            <?php 
              $id_pegawai = $_SESSION['pegawai'];
              $pegawai = query("SELECT * FROM pegawai WHERE id_pegawai = $id_pegawai")[0];
            ?>
            Hi, <?= $pegawai["nama_pegawai"]; ?>
          </a>
          <div class="dropdown-menu">
            <a href="../index.php" class="dropdown-item">Back To Home</a>
            <div class="dropdown-divider"></div>
            <a href="../logout.php" class="dropdown-item">logout</a>
          </div>
        </li>
      </ul>

      <!-- mobile app -->
      <ul class="navbar-nav d-block d-lg-none">
        <li class="nav-item">
          <a href="" class="nav-link"> Hi, <?= $pegawai["nama_pegawai"]; ?></a>
        </li>
      </ul>
    </div>
  </div>
</nav>
  <div class="section-content section-dashboard-home" data-aos="fade-up">
    <div class="container-fluid">
      <div class="dashboard-heading">
      <?php 
        $transactionMain = query("SELECT * FROM transaksi_produk INNER JOIN 
        user ON transaksi_produk.id_user = user.id_user WHERE transaksi_produk.id_transaksiproduk = $idTransaction")[0];
      ?>
        <h2 class="dashboard-title"><?= $transactionMain["nama"]; ?></h2>
        <p class="dashboard-subtitle">#<?= $transactionMain["code"]; ?></p>
      </div>
      <div class="dashboard-content store-cart">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
              <form action="" method="POST" enctype="multipart/form-data">
                <div class="row" data-aos="fade-up" data-aos-delay="100">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="foto_jemput">Foto Penjemputan</label>
                        <input type="hidden" name="id_transaksiproduk" value="<?= $transactionMain["id_transaksiproduk"]; ?>">
                        <input type="file" name="foto_jemput" id="foto_jemput" class="form-control-file">
                        <div class="text-right">
                          <button type="submit" name="fotojemput" class="btn btn-success mt-2">Save</button>
                        </div>
                      </div>
                    </div>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  



