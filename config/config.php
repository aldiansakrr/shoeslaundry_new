<?php 
session_start();
// Database
// sql212.epizy.com
// epiz_27842841
// V3XtbZ0vLKSGuC1
// epiz_27842841_daging_online
$host = 'localhost';
$user = 'root';
$password = '';
$dbname = '_shoeslaundry';

$conn = mysqli_connect($host, $user, $password, $dbname);


// query
function query($query)
{
    global $conn;
    $result = mysqli_query($conn, $query);
    $rows = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }

    return $rows;
}

// Jumlah Data
function rows($query)
{
    global $conn;
    $result = mysqli_query($conn, $query);
    $rows = mysqli_num_rows($result);
    return $rows;
}

// Data Produk
function tambahProduk($data)
{
    global $conn;
    $nama_produk = $data["nama_produk"];
    $harga_produk = $data["harga_produk"];
    $stok_produk = $data["stok_produk"];
    $deskripsi_produk = $data["deskripsi_produk"];
    $foto_produk = $_FILES['foto_produk']['name'];

    if($foto_produk != ""){
        $ekstensi_diperbolehkan = array('png','jpg','jpeg','webp');
        $x = explode('.', $foto_produk);
        $ekstensi = strtolower(end($x));
        $file_tmp = $_FILES['foto_produk']['tmp_name'];
        $angka_acak = rand(1,999);
        $nama_gambar_baru = $angka_acak.'-'.$foto_produk;
        if(in_array($ekstensi, $ekstensi_diperbolehkan) === true){
            move_uploaded_file($file_tmp, 'produk/'.$nama_gambar_baru);
            $query = "INSERT INTO produk VALUES(NULL,'$nama_produk','$harga_produk','$deskripsi_produk','$stok_produk','$nama_gambar_baru')";
            mysqli_query($conn, $query);
            return mysqli_affected_rows($conn);
        }
    }


    // $query = "INSERT INTO produk
    //             VALUES
    //             (NULL,, '$nama_produk', '$harga_produk', '$deskripsi_produk', '$stok_produk', '$category', '$stock')
    //         ";

    // mysqli_query($conn, $query);
    // return mysqli_affected_rows($conn);
}

function updateProduk($data)
{
    global $conn;

    $id_produk = $data["id_produk"];
    $nama_produk = $data["nama_produk"];
    $harga_produk = $data["harga_produk"];
    $deskripsi_produk = $data["deskripsi_produk"];
    $stok_produk = $data["stok_produk"];

    $query = "UPDATE produk SET
                nama_produk = '$nama_produk',
                harga_produk = '$harga_produk',
                deskripsi_produk = '$deskripsi_produk',
                stok_produk = '$stok_produk'
                WHERE id_produk = $id_produk
            ";

    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function hapusProduk($id_produk) 
{
    global $conn;
    $data = mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM produk WHERE id_produk = '$id_produk'"));
    if(is_file("../admin/produk/".$data['foto_produk'])){
        unlink("../admin/produk/".$data['foto_produk']);
    }
    mysqli_query($conn, "DELETE FROM produk WHERE id_produk = $id_produk");
    return mysqli_affected_rows($conn);

}

// Data Jasa
function tambahJasa($data)
{
    global $conn;
    $nama_jasa = $data["nama_jasa"];
    $harga_jasa = $data["harga_jasa"];
    $jenis_jasa = $data["jenis_jasa"];
    $deskripsi_jasa = $data["deskripsi_jasa"];
    $foto_jasa = $_FILES['foto_jasa']['name'];

    if($foto_jasa != ""){
        $ekstensi_diperbolehkan = array('png','jpg','jpeg','webp');
        $x = explode('.', $foto_jasa);
        $ekstensi = strtolower(end($x));
        $file_tmp = $_FILES['foto_jasa']['tmp_name'];
        $angka_acak = rand(1,999);
        $nama_gambar_baru = $angka_acak.'-'.$foto_jasa;
        if(in_array($ekstensi, $ekstensi_diperbolehkan) === true){
            move_uploaded_file($file_tmp, 'picjasa/'.$nama_gambar_baru);
            $query = "INSERT INTO jasa VALUES(NULL,'$nama_jasa','$harga_jasa','$jenis_jasa','$deskripsi_jasa','$nama_gambar_baru')";
            mysqli_query($conn, $query);
            return mysqli_affected_rows($conn);
        }
    }

}

function updateJasa($data)
{
    global $conn;

    $id_jasa = $data["id_jasa"];
    $nama_jasa = $data["nama_jasa"];
    $harga_jasa = $data["harga_jasa"];
    $jenis_jasa = $data["jenis_jasa"];
    $deskripsi_jasa = $data["deskripsi_jasa"];

    $query = "UPDATE jasa SET
                nama_jasa = '$nama_jasa',
                harga_jasa = '$harga_jasa',
                jenis_jasa = '$jenis_jasa',
                deskripsi_jasa = '$deskripsi_jasa'
                WHERE id_jasa = $id_jasa
            ";

    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function hapusJasa($id_jasa) 
{
    global $conn;
    $data = mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM jasa WHERE id_jasa = '$id_jasa'"));
    if(is_file("../admin/picjasa/".$data['foto_jasa'])){
        unlink("../admin/picjasa/".$data['foto_jasa']);
    }
    mysqli_query($conn, "DELETE FROM jasa WHERE id_jasa = $id_jasa");
    return mysqli_affected_rows($conn);

}
// Data Pembayaran

function tambahPembayaran($data)
{
    global $conn;
    $nama_pembayaran = $data["nama_pembayaran"];
    $nomor = $data["nomor"];
    $atas_nama = $data["atas_nama"];
    

    $query = "INSERT INTO pembayaran
                VALUES
                (NULL, '$nama_pembayaran', '$nomor','$atas_nama')
            ";

    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function updatePembayaran($data)
{
    global $conn;

    $id_pembayaran = $data["id_pembayaran"];
    $nama_pembayaran = $data["nama_pembayaran"];
    $nomor = $data["nomor"];
    $atas_nama = $data["atas_nama"];

    $query = "UPDATE pembayaran SET
                nama_pembayaran = '$nama_pembayaran',
                nomor = '$nomor',
                atas_nama = '$atas_nama'
                WHERE id_pembayaran = $id_pembayaran
            ";

    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function hapusPembayaran($id) 
{
    global $conn;

    mysqli_query($conn, "DELETE FROM pembayaran WHERE id_pembayaran = $id");
    return mysqli_affected_rows($conn);

}
// Data Gallery

function tambahGaleri($data)
{
    global $conn;
    $name = $data["name"];
    $photo = upload();
    if (!$photo) {
        return false;
    }

    $query = "INSERT INTO products_galleries
                VALUES
                ('', '$photo', '$name')
            ";

    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function updateGaleri($data)
{
    global $conn;

    $id = $data["id"];
    $name = $data["name"];
    $photoLama = $data["photoLama"];
    if ($_FILES['photo']['error'] === 4) {
        $photo = $photoLama;
    } else {
        $photo = upload();
        if (!$photo) {
            return false;
        }
    }

    $query = "UPDATE products_galleries SET
                photos = '$photo',
                product_id = '$name'
                WHERE id_gallery = $id
            ";

    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function hapusGallery($id) 
{
    global $conn;

    mysqli_query($conn, "DELETE FROM products_galleries WHERE id_gallery = $id");
    return mysqli_affected_rows($conn);

}

// Category

function tambahCategory($data)
{
    global $conn;
    $name = $data["name"];
    $slug = slug($name);

    $query = "INSERT INTO categories
                VALUES
                ('', '$name', '$slug')
            ";

    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function updateCategory($data)
{
    global $conn;

    $id = $data["id"];
    $name = $data["name"];
    $slug = slug($name);

    $query = "UPDATE categories SET
                category_name = '$name',
                slug = '$slug'
                WHERE id = $id
            ";

    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function hapusCategories($id)
{
    global $conn;

    mysqli_query($conn, "DELETE FROM categories WHERE id = $id");
    return mysqli_affected_rows($conn);
}

// Users

function tambahUser($data)
{
    global $conn;
    $nama = $data["nama"];
    $email = stripslashes($data["email"]);
    $no_hp = $data["no_hp"];
    $alamat = $data["alamat"];
    $password = password_hash($data["password"], PASSWORD_DEFAULT);
    $role = $data["role"];

    $query = "INSERT INTO user
                VALUES
                (NULL, '$nama', '$email', '$password', '$no_hp', '$alamat' '$role')
            ";

    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function updateUser($data)
{
    global $conn;
    $id = $data["id_user"];
    $result = query("SELECT password FROM user WHERE id_user = $id")[0];
    $nama = $data["nama"];
    $email = $data["email"];
    $no_hp = $data["no_hp"];
    if (empty($data["password"])) {
        $password = $result["password"];
    } else {
        $password = password_hash($data["password"], PASSWORD_DEFAULT);
    }
    $alamat = $data["alamat"];
    $role = $data["role"];

    $query = "UPDATE user SET
                nama = '$nama',
                email = '$email',
                password = '$password',
                no_hp = '$no_hp',
                alamat = '$alamat',
                role = '$role'
                WHERE id_user = $id
            ";

    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);   
}

function hapusUser($id)
{
    global $conn;

    mysqli_query($conn, "DELETE FROM user WHERE id_user = $id");
    return mysqli_affected_rows($conn);
}

// Register

function register($data)
{
    global $conn;

    $nama = $data["nama"];
    $email = $data["email"];
    $password = mysqli_real_escape_string($conn, $data["password"]);
    $password2 = mysqli_real_escape_string($conn, $data["password2"]);
    $alamat = $data["alamat"];
    $no_hp = $data["no_hp"];
    

    $result = mysqli_query($conn, "SELECT email FROM user WHERE email = '$email'");
    if (mysqli_fetch_assoc($result)) {
        $_SESSION["error"] = "Email Sudah Tersedia!";
        return false;
    }

    if ($password !== $password2) {
        $_SESSION["error"] = "Konfirmasi Password Tidak Sesuai";
        return false;
    }

    $password = password_hash($password, PASSWORD_DEFAULT);

    $query = "INSERT INTO user
                (id_user,nama,email,password,no_hp,alamat,role)
                VALUES
                (NULL,'$nama','$email','$password','$no_hp','$alamat','USER');
            ";

    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

// Add To cart
function addToCart($data) 
{
    global $conn;
    
    

    if(isset($data["id_produk"])){
    $id_produk = $data["id_produk"];
    $id_user = $data["id_user"];
    $jumlah = $data["jumlah"];

    $produk = query("SELECT * FROM produk WHERE id_produk = $id_produk")[0];
    $stok = $produk["stok_produk"];
    $sisaBarang = $stok - $jumlah;

    if ($sisaBarang < 0) {
        echo "<script>
                alert('Stock Barang Tidak Cukup');
            </script>";
        return false;
    }

    $total = $jumlah * $produk["harga_produk"];

    $queryCart = "INSERT INTO keranjang
                VALUES
                (NULL, '$id_user', '$id_produk', NULL , '$jumlah', '$total')
            ";
    
    mysqli_query($conn, $queryCart);

    $stockProduk =  $produk["stok_produk"] - $jumlah;

    $queryProduct = "UPDATE produk SET
                        stok_produk = '$stockProduk'
                    WHERE id_produk = $id_produk
                    ";
    mysqli_query($conn, $queryProduct);
    return mysqli_affected_rows($conn);

    }else if(isset($data["id_jasa"])){
        $id_jasa = $data["id_jasa"];
        $id_user = $data["id_user"];
        $jumlah = $data["jumlah"];
        $jasa = query("SELECT * FROM jasa WHERE id_jasa = $id_jasa")[0];
        $total = $jumlah * $jasa["harga_jasa"];

        $queryCart = "INSERT INTO keranjang
                    VALUES
                    (NULL, '$id_user', NULL , '$id_jasa', '$jumlah', '$total')
                ";
        
        mysqli_query($conn, $queryCart);
        return mysqli_affected_rows($conn);
    }
    

}

// Delete Product at cart

function deleteProductAtCart($data)
{
    global $conn;
    $idProduct = $data["deleteProduct"];

    mysqli_query($conn, "DELETE FROM keranjang WHERE id_keranjang = $idProduct");
    return mysqli_affected_rows($conn);
}

// checkout

function checkout($data)
{
    global $conn;

    $code = "EZM-";
    $id_user = $data['id_user'];
    $total = $data["total"];
    if ($total == 0) {
        echo "<script>
                alert('Pilih Produk Terlebih dahulu');
            </script>";
        return false;
    }
    if (!isset($data["alamat"])) {
        $alamat = $data["alamat"];
    }else {
        $alamat = $data["alamat"];
    }

    if (empty($alamat)) {
        echo "<script>
                alert('Masukkan Alamat Terlebih dahulu');
            </script>";
        return false;
    }

    $no_hp = $data["no_hp"];
    $pembayaran = $data["pembayaran"];
    $status = "BELUM KONFIRMASI";
    $jemput = isset($data["jemput"]) ? $data["jemput"] : 0;
    $foto_jemput = "";
    $terkirim = isset($data["terkirim"] ) ? $data["terkirim"] : 0;
    $photo = "";
    $code .= mt_rand(00000, 99999);
    $penerima = "";
    $foto_penerimaan = "";
    $queryUser = "UPDATE user SET
                    no_hp = '$no_hp',
                    alamat = '$alamat'
                    WHERE id_user = $id_user
                ";
    mysqli_query($conn, $queryUser);

    $queryTransaction = "INSERT INTO transaksi_produk
                            VALUES
                            (NULL, 
                            '$id_user', 
                            '$total', 
                            '$pembayaran', 
                            '$status', 
                            '$jemput' ,
                            '$foto_jemput', 
                            '$terkirim', 
                            '$photo', 
                            '$code', 
                            '$penerima',
                            '$foto_penerimaan', 
                            NOW(),
                            NULL)
                        ";
    mysqli_query($conn, $queryTransaction);
    
    $carts = query("SELECT * FROM keranjang INNER JOIN produk ON keranjang.id_produk = produk.id_produk WHERE id_user = $id_user");
    $codeProduct = 'PRD-';
    $jasas = query("SELECT * FROM keranjang INNER JOIN jasa ON keranjang.id_jasa = jasa.id_jasa WHERE id_user = $id_user");
    $dataIdTransaction = query("SELECT * FROM transaksi_produk ORDER BY id_transaksiproduk DESC LIMIT 1");
    $codeProduct .= mt_rand(00000,99999);
    
    if($carts) { foreach ($carts as $cart) {
    foreach ($dataIdTransaction as $idTransaction) {
        $transaction_id = $idTransaction["id_transaksiproduk"];
    }
    $id_product = $cart["id_produk"];
    
    $productPrice = $cart["harga_produk"];
    $banyak = $cart["jumlah"];
    $queryTransactionDetails = "INSERT INTO detail_transaksiproduk
                            VALUES
                        (NULL, '$transaction_id', '$id_product', NULL, '$productPrice', '$banyak', '$codeProduct')
                        ";
    mysqli_query($conn, $queryTransactionDetails);
    } }else
    if($jasas) { foreach ($jasas as $jasa) {
        foreach ($dataIdTransaction as $idTransaction) {
            $transaction_id = $idTransaction["id_transaksiproduk"];
        }
        $id_jasa = $jasa["id_jasa"];
        
        $productPrice = $jasa["harga_jasa"];
        $banyak = $jasa["jumlah"];
        $queryTransactionDetails = "INSERT INTO detail_transaksiproduk
                                VALUES
                            (NULL, '$transaction_id', NULL , '$id_jasa', '$productPrice', '$banyak', '$codeProduct')
                            ";
        mysqli_query($conn, $queryTransactionDetails);
        } }
    mysqli_query($conn, "DELETE FROM keranjang WHERE id_user = $id_user");

    return mysqli_affected_rows($conn);

}

// transfer
function uploadTransfer($data)
{
    global $conn;

    $id = $data["id_transaksiproduk"];
    $photo = upload();
    if (!$photo) {
        return false;
    }

    $query = "UPDATE transaksi_produk SET
                foto_transaksiproduk = '$photo'
                WHERE id_transaksiproduk = $id
            ";

    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

// foto jemput

function fotoJemput($data)
{
    global $conn;

    $id = $data["id_transaksiproduk"];
    $foto_jemput = uploadJemput();
    if (!$foto_jemput) {
        return false;
    }

    $query = "UPDATE transaksi_produk SET
                foto_jemput = '$foto_jemput',
                status = 'PICKUP'
                WHERE id_transaksiproduk = $id
            ";

    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function konfirmasi($data)
{
    global $conn;

    $id = $data["id_transaksiproduk"];
    $status = "TERKONFIRMASI";

    $query = "UPDATE transaksi_produk SET
                status = '$status'
                WHERE id_transaksiproduk = $id
            ";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function proses($data)
{
    global $conn;

    $id = $data["id_transaksiproduk"];
    $status = "PROSES";

    $query = "UPDATE transaksi_produk SET
                status = '$status'
                WHERE id_transaksiproduk = $id
            ";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function selesai($data)
{
    global $conn;

    $id = $data["id_transaksiproduk"];
    $status = "SELESAI";

    $query = "UPDATE transaksi_produk SET
                status = '$status'
                WHERE id_transaksiproduk = $id
            ";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function hapusTransaction($id)
{
    global $conn;
    $data = mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM transaksi_produk WHERE id_transaksiproduk = '$id'"));
    if(is_file("../admin/bukti_transaksiproduk/".$data['foto_transaksiproduk'])){
        unlink("../admin/bukti_transaksiproduk/".$data['foto_transaksiproduk']);
    }
    if(is_file("../admin/foto_jemput/".$data['foto_jemput'])){
        unlink("../admin/foto_jemput/".$data['foto_jemput']);
    }
    if(is_file("../admin/foto_penerimaan/".$data['foto_penerimaan'])){
        unlink("../admin/foto_penerimaan/".$data['foto_penerimaan']);
    }
    mysqli_query($conn, "DELETE FROM transaksi_produk WHERE id_transaksiproduk = $id");
    mysqli_query($conn, "DELETE FROM detail_transaksiproduk WHERE id_transaksiproduk = $id");
    
    return mysqli_affected_rows($conn);
}

// pegawai

function tambahPegawai($data)
{
    global $conn;
    $nama_pegawai = $data["nama_pegawai"];
    $email_pegawai = stripslashes($data["email_pegawai"]);
    $password_pegawai = password_hash($data["password_pegawai"], PASSWORD_DEFAULT);
    $telp_pegawai = $data["telp_pegawai"];
    $no_pegawai = $data["no_pegawai"];

    $query = "INSERT INTO pegawai
                VALUES
                (NULL, '$nama_pegawai', '$email_pegawai', '$password_pegawai', '$telp_pegawai', '$no_pegawai')
            ";

    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function updatePegawai($data)
{
    global $conn;
    $id_pegawai = $data["id_pegawai"];
    $result = query("SELECT password_pegawai FROM pegawai WHERE id_pegawai = $id_pegawai")[0];
    $nama_pegawai = $data["nama_pegawai"];
    $telp_pegawai = $data["telp_pegawai"];
    if (empty($data["password_pegawai"])) {
        $password_pegawai = $result["password_pegawai"];
    } else {
        $password_pegawai = password_hash($data["password_pegawai"], PASSWORD_DEFAULT);
    }
    $no_pegawai = $data["no_pegawai"];

    $query = "UPDATE pegawai SET
                nama_pegawai = '$nama_pegawai',
                password_pegawai = '$password_pegawai',
                telp_pegawai = '$telp_pegawai',
                no_pegawai = '$no_pegawai'
                WHERE id_pegawai = $id_pegawai
            ";

    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function hapusPegawai($id)
{
    global $conn;

    mysqli_query($conn, "DELETE FROM pegawai WHERE id_pegawai = $id");
    return mysqli_affected_rows($conn);
}

// pickup
function pickup($data)
{
    global $conn;

    $id_transaksiproduk = $data["id_transaksiproduk"];
    date_default_timezone_set('Asia/jakarta');
    // $tambah3Jam = time() + 60 * 60 * 3;
    // $arrive = date('Y-m-d H:i:s', $tambah3Jam);

    $query = "UPDATE transaksi_produk SET
                status = 'PICKUP'
                WHERE id_transaksiproduk = $id_transaksiproduk
            ";
    
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function terkirim($data)
{
    global $conn;

    $id_transaksiproduk = $data["id_transaksiproduk"];
    $penerima = $data["penerima"];

    $query = "UPDATE transaksi_produk SET
                status = 'TERKIRIM',
                penerima = '$penerima'
                WHERE id_transaksiproduk = $id_transaksiproduk
            ";
    
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function terkirimGambar($data)
{
    global $conn;

    $id_transaksiproduk = $data["id_transaksiproduk"];
    $penerima = $data["penerima"];
    $foto_penerimaan = uploadPenerimaan();
    $arrived_at = $data["arrived_at"];
    if(!$foto_penerimaan){
        return false;
    }
    $query = "UPDATE transaksi_produk SET
                status = 'TERKIRIM',
                penerima = '$penerima',
                foto_penerimaan = '$foto_penerimaan',
                arrived_at = '$arrived_at'
                WHERE id_transaksiproduk = $id_transaksiproduk
            ";
    
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

// Rekening
function tambahRekening($data)
{
    global $conn;
    $bank_name = $data["bank_name"];
    $no_rekening = stripslashes($data["no_rekening"]);
    $pemilik = $data["pemilik"];

    $query = "INSERT INTO rekening_numbers
                VALUES
                ('', '$bank_name', '$no_rekening', '$pemilik')
            ";

    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function updateRekening($data)
{
    global $conn;
    $id = $data["id"];
    $bank_name = $data["bank_name"];
    $no_rekening = $data["no_rekening"];
    $pemilik = $data["pemilik"];

    $query = "UPDATE rekening_numbers SET
                bank_name = '$bank_name',
                number = '$no_rekening',
                rekening_name = '$pemilik'
                WHERE id_rekening = $id
            ";

    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn); 
}

function hapusRekening($id)
{
    global $conn;
    mysqli_query($conn, "DELETE FROM rekening_numbers WHERE id_rekening = $id");
    return mysqli_affected_rows($conn);
}

function upload()
{
    $namaFile = $_FILES['photo']["name"];
    $size = $_FILES['photo']["size"];
    $error = $_FILES['photo']["error"];
    $tmpName = $_FILES['photo']["tmp_name"];

    if ($error === 4) {
        echo "<script>
            alert('Pilih Gambar Terlebih dahulu');
          </script>";
        return false;
    }

    $ekstensiGambarValid = ['png', 'jpg', 'jpeg'];
    $ekstensiGambar = explode('.', $namaFile);
    $ekstensiGambar = strtolower(end($ekstensiGambar));
    if (!in_array($ekstensiGambar, $ekstensiGambarValid)) {
        echo "<script>
                    alert('format gambar Harus (png, jpg atau jpeg)');
                </script>";
        return false;
    }

    if ($size > 10000000) {
        echo "<script>
                    alert('Ukuran file gambar harus < 10 Mb');
                </script>";
        return false;
    }

    $namaFileBaru = uniqid();
    $namaFileBaru .= '.';
    $namaFileBaru .= $ekstensiGambar;
    move_uploaded_file($tmpName, '../admin/bukti_transaksiproduk/' . $namaFileBaru);
    return $namaFileBaru;

}

function uploadJemput()
{
    $namaFile = $_FILES['foto_jemput']["name"];
    $size = $_FILES['foto_jemput']["size"];
    $error = $_FILES['foto_jemput']["error"];
    $tmpName = $_FILES['foto_jemput']["tmp_name"];

    if ($error === 4) {
        echo "<script>
            alert('Pilih Gambar Terlebih dahulu');
          </script>";
        return false;
    }

    $ekstensiGambarValid = ['png', 'jpg', 'jpeg'];
    $ekstensiGambar = explode('.', $namaFile);
    $ekstensiGambar = strtolower(end($ekstensiGambar));
    if (!in_array($ekstensiGambar, $ekstensiGambarValid)) {
        echo "<script>
                    alert('format gambar Harus (png, jpg atau jpeg)');
                </script>";
        return false;
    }

    if ($size > 10000000) {
        echo "<script>
                    alert('Ukuran file gambar harus < 10 Mb');
                </script>";
        return false;
    }

    $namaFileBaru = uniqid();
    $namaFileBaru .= '.';
    $namaFileBaru .= $ekstensiGambar;
    move_uploaded_file($tmpName, '../admin/foto_jemput/' . $namaFileBaru);
    return $namaFileBaru;

}

function uploadPenerimaan()
{
    $namaFile = $_FILES['foto_penerimaan']["name"];
    $size = $_FILES['foto_penerimaan']["size"];
    $error = $_FILES['foto_penerimaan']["error"];
    $tmpName = $_FILES['foto_penerimaan']["tmp_name"];

    if ($error === 4) {
        echo "<script>
            alert('Pilih Gambar Terlebih dahulu');
          </script>";
        return false;
    }

    $ekstensiGambarValid = ['png', 'jpg', 'jpeg'];
    $ekstensiGambar = explode('.', $namaFile);
    $ekstensiGambar = strtolower(end($ekstensiGambar));
    if (!in_array($ekstensiGambar, $ekstensiGambarValid)) {
        echo "<script>
                    alert('format gambar Harus (png, jpg atau jpeg)');
                </script>";
        return false;
    }

    if ($size > 10000000) {
        echo "<script>
                    alert('Ukuran file gambar harus < 10 Mb');
                </script>";
        return false;
    }

    $namaFileBaru = uniqid();
    $namaFileBaru .= '.';
    $namaFileBaru .= $ekstensiGambar;
    move_uploaded_file($tmpName, '../admin/foto_penerimaan/' . $namaFileBaru);
    return $namaFileBaru;

}

function slug($text) 
{
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);

    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    $text = preg_replace('~[^-\w]+~', '', $text);

    $text = trim($text, '-');

    $text = preg_replace('~-+~', '-', $text);

    $text = strtolower($text);

    return $text;
}

?>