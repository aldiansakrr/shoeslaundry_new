<?php 

$id_pembayaran = $_GET["id_pembayaran"];

if (hapusPembayaran($id_pembayaran) > 0) {
    echo "<script>
            alert('Pembayaran Berhasil Dihapus');
            document.location.href = '?page=pembayaran';
        </script>";
} else {
    echo "<script>
            alert('Pembayaran Gagal Dihapus');
            document.location.href = '?page=pembayaran';
        </script>";
}

?>