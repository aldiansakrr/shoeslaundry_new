<?php

$id_pegawai = $_GET["id_pegawai"];

$pegawai = query("SELECT * FROM pegawai WHERE id_pegawai = $id_pegawai")[0];

if (isset($_POST["updatePegawai"])) {
  if (updatePegawai($_POST) > 0) {
    echo "<script>
            alert('Pegawai Berhasil Diubah');
            document.location.href = '?page=pegawai';
          </script>";
  } else {
    echo "<script>
            alert('Pegawai Gagal Diubah');
            document.location.href = '?page=pegawai';
          </script>";
  }
}

?>
<nav
  class="navbar navbar-expand-lg navbar-light navbar-store fixed-top"
  data-aos="fade-down"
>
  <div class="container-fluid">
    <button
      class="btn btn-secondary d-md-none mr-auto mr-2"
      id="menu-toggle"
    >
      &laquo; Menu
    </button>
    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarResponsive"
    >
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collpase navbar-collapse" id="navbarResponsive">
      <!-- dekstop menu -->
      <ul class="navbar-nav d-none d-lg-flex ml-auto">
        <li class="nav-item dropdown">
          <a
            href="#"
            class="nav-link"
            id="navbarDropdown"
            role="button"
            data-toggle="dropdown"
          >
          <img
              src="../assets/images/person-circle.svg"
              alt="profile"
              height="40px"
              class="rounded-circle mr-2 profile-picture"
            />
            <?php 
              $id_user = $_SESSION['user'];
              $user = query("SELECT * FROM user WHERE id_user = $id_user")[0];
            ?>
            Hi, <?= $user["nama"]; ?>
          </a>
          <div class="dropdown-menu">
            <a href="../index.php" class="dropdown-item">Back To Home</a>
            <div class="dropdown-divider"></div>
            <a href="../logout.php" class="dropdown-item">logout</a>
          </div>
        </li>
      </ul>

      <!-- mobile app -->
      <ul class="navbar-nav d-block d-lg-none">
        <li class="nav-item">
          <a href="" class="nav-link"> Hi, <?= $user["nama"]; ?></a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<div
  class="section-content section-dashboard-home"
  data-aos="fade-up"
>
  <div class="container-fluid">
    <div class="dashboard-heading">
      <h2 class="dashboard-title"><?= $pegawai["nama_pegawai"]; ?></h2>
      <p class="dashboard-subtitle">Pegawai Details</p>
    </div>
    <div class="dashboard-content">
      <div class="row">
        <div class="col-12 mt-2">
          <form action="" method="POST">
            <div class="card">
              <div class="card-body">
                  <div class="row">
                    <div class="col-md-12">
                    <input type="hidden" name="id_pegawai" value="<?= $pegawai["id_pegawai"]; ?>">
                      <div class="form-group">
                        <label for="nama_pegawai">Nama Pegawai</label>
                        <input
                          type="text"
                          name="nama_pegawai"
                          id="nama_pegawai"
                          class="form-control"
                          value="<?= $pegawai["nama_pegawai"]; ?>"
                          required
                        />
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="email_pegawai">Email Pegawai</label>
                        <input
                          type="text"
                          name="email_pegawai"
                          id="email_pegawai"
                          class="form-control"
                          value="<?= $pegawai["email_pegawai"]; ?>"
                          readonly
                        />
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="password_pegawai">Password</label>
                        <input
                          type="password"
                          name="password_pegawai"
                          id="password_pegawai"
                          class="form-control"
                        />
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="telp_pegawai">No HP / WA</label>
                        <input
                          type="number"
                          name="telp_pegawai"
                          id="telp_pegawai"
                          class="form-control"
                          value="<?= $pegawai["telp_pegawai"]; ?>"
                          required
                        />
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="no_pegawai">No Pegawai</label>
                        <input
                          type="number"
                          name="no_pegawai"
                          id="no_pegawai"
                          class="form-control"
                          value="<?= $pegawai["no_pegawai"]; ?>"
                          required
                        />
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12 text-right">
                      <button
                        type="submit"
                        name="updatePegawai"
                        class="btn btn-success px-4"
                      >
                        Save Now
                      </button>
                    </div>
                  </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>