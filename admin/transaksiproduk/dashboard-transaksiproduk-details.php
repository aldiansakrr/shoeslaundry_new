<?php 
$id = $_SESSION["user"];
$user = query("SELECT * FROM user WHERE id_user = $id")[0];

$id_transaksiproduk = $_GET["id_transaksiproduk"];
$produk_transaction_details = query("SELECT * FROM detail_transaksiproduk INNER JOIN transaksi_produk ON detail_transaksiproduk.id_transaksiproduk = transaksi_produk.id_transaksiproduk INNER JOIN produk ON detail_transaksiproduk.id_produk = produk.id_produk WHERE detail_transaksiproduk.id_transaksiproduk = $id_transaksiproduk");
$jasa_transaction_details = query("SELECT * FROM detail_transaksiproduk INNER JOIN transaksi_produk ON detail_transaksiproduk.id_transaksiproduk = transaksi_produk.id_transaksiproduk INNER JOIN jasa ON detail_transaksiproduk.id_jasa = jasa.id_jasa WHERE detail_transaksiproduk.id_transaksiproduk = $id_transaksiproduk");

if (isset($_POST["konfirmasi"])) {
  if (konfirmasi($_POST) > 0) {
    header("Location: ?page=transaksiproduk");
  }
}

if (isset($_POST["proses"])) {
  if (proses($_POST) > 0) {
    header("Location: ?page=transaksiproduk");
  }
}

if (isset($_POST["selesai"])) {
  if (selesai($_POST) > 0) {
    header("Location: ?page=transaksiproduk");
  }
}

?>
  <nav
  class="navbar navbar-expand-lg navbar-light navbar-store fixed-top"
  data-aos="fade-down"
>
  <div class="container-fluid">
    <button
      class="btn btn-secondary d-md-none mr-auto mr-2"
      id="menu-toggle"
    >
      &laquo; Menu
    </button>
    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarResponsive"
    >
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collpase navbar-collapse" id="navbarResponsive">
      <!-- dekstop menu -->
      <ul class="navbar-nav d-none d-lg-flex ml-auto">
        <li class="nav-item dropdown">
          <a
            href="#"
            class="nav-link"
            id="navbarDropdown"
            role="button"
            data-toggle="dropdown"
          >
          <img
              src="../assets/images/person-circle.svg"
              alt="profile"
              height="40px"
              class="rounded-circle mr-2 profile-picture"
            />
            <?php 
              $id_user = $_SESSION['user'];
              $user = query("SELECT * FROM user WHERE id_user = $id_user")[0];
            ?>
            Hi, <?= $user["nama"]; ?>
          </a>
          <div class="dropdown-menu">
            <a href="../index.php" class="dropdown-item">Back To Home</a>
            <div class="dropdown-divider"></div>
            <a href="../logout.php" class="dropdown-item">logout</a>
          </div>
        </li>
      </ul>

      <!-- mobile app -->
      <ul class="navbar-nav d-block d-lg-none">
        <li class="nav-item">
          <a href="" class="nav-link"> Hi, <?= $user["nama"]; ?></a>
        </li>
      </ul>
    </div>
  </div>
</nav>
  <div class="section-content section-dashboard-home" data-aos="fade-up">
    <div class="container-fluid">
      <div class="dashboard-heading">
      <?php 
        $transactionMain = query("SELECT * FROM transaksi_produk INNER JOIN user ON transaksi_produk.id_user = user.id_user WHERE transaksi_produk.id_transaksiproduk = $id_transaksiproduk")[0];
      ?>
        <h2 class="dashboard-title"><?= $transactionMain["nama"]; ?></h2>
        <p class="dashboard-subtitle">#<?= $transactionMain["code"]; ?></p>
      </div>
      <div class="dashboard-content store-cart">
        <div class="row" data-aos="fade-up" data-aos-delay="100">
            <div class="col-12 table-responsive">
              <table class="table table-borderless table-cart">
                <thead>
                  <tr>
                    <th>Image</th>
                    <th>Name &amp; Seller</th>
                    <th>Kode Produk</th>
                    <th>Banyak</th>
                    <th>Price</th>
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody>
                <?php 
                $jumlah = 0;
                $total = 0;
                ?>
                  <?php if(isset($produk_transaction_details)) { foreach ($produk_transaction_details as $t) : ?>
                    <?php 
                      $idProduct = $t["id_produk"];
                      $product = query("SELECT * FROM produk WHERE produk.id_produk = $idProduct");
                      // $gallery = query("SELECT * FROM products_galleries INNER JOIN products ON products_galleries.product_id = products.id_product WHERE products_galleries.product_id = $idProduct");
                      $jumlah += $t["jumlah"];
                      $total = $t["harga"] * $t["jumlah"];
                    ?>
                    <tr>
                      <td style="width: 10%;">
                        <img src="../admin/produk/<?= $t["foto_produk"]; ?>" alt="" class="cart-image w-100">
                      </td>
                      <td style="width: 20%;">
                        <div class=""><?= $t["nama_produk"]; ?></div>
                        <!-- <div class="product-title"><?= $t["unit"] / 1000; ?> Kilogram</div> -->
                      </td>
                      <td style="width: 20%;">
                        <div class="">#<?= $t["kode_produk"]; ?></div>
                      </td>
                      <td style="width: 10%;">
                        <div class=""><?= $t["jumlah"]; ?></div>
                      </td>
                      <td style="width: 20%;">
                        <div class="">Rp. <?= number_format($t["harga"]); ?></div>
                        <div class="product-title">IDR</div>
                      </td>
                      <td style="width: 20%;">
                        <div class="">Rp. <?= number_format($total); ?></div>
                        <div class="product-title">IDR</div>
                      </td>
                    </tr>
                  <?php endforeach; } ?>

                  <?php if(isset($jasa_transaction_details)) { foreach ($jasa_transaction_details as $j) : ?>
                    <?php 
                      $idJasa = $j["id_jasa"];
                      $product = query("SELECT * FROM jasa WHERE jasa.id_jasa = $idJasa");
                      // $gallery = query("SELECT * FROM products_galleries INNER JOIN products ON products_galleries.product_id = products.id_product WHERE products_galleries.product_id = $idProduct");
                      $jumlah += $j["jumlah"];
                      $total = $j["harga"] * $j["jumlah"];
                    ?>
                    <tr>
                      <td style="width: 10%;">
                        <img src="../admin/picjasa/<?= $j["foto_jasa"]; ?>" alt="" class="cart-image w-100">
                      </td>
                      <td style="width: 20%;">
                        <div class=""><?= $j["nama_jasa"]; ?></div>
                        <!-- <div class="product-title"><?= $t["unit"] / 1000; ?> Kilogram</div> -->
                      </td>
                      <td style="width: 20%;">
                        <div class="">#<?= $j["kode_produk"]; ?></div>
                      </td>
                      <td style="width: 10%;">
                        <div class=""><?= $j["jumlah"]; ?></div>
                      </td>
                      <td style="width: 20%;">
                        <div class="">Rp. <?= number_format($j["harga"]); ?></div>
                        <div class="product-title">IDR</div>
                      </td>
                      <td style="width: 20%;">
                        <div class="">Rp. <?= number_format($total); ?></div>
                        <div class="product-title">IDR</div>
                      </td>
                    </tr>
                  <?php endforeach; } ?>
                </tbody>
              </table>
            </div>
        </div>
        <form action="" method="POST">
          <div class="row" data-aos="fade-up" data-aos-delay="150">
            <div class="col-12">
              <hr>
            </div>
            <div class="col-12">
              <h2>
                Shipping Details
              </h2>
            </div>
          </div>
          <div class="row mb-2" data-aos="fade-up" data-aos-delay="200">
            <div class="col-md-12">
              <div class="form-group">
                <label for="no_hp">No HP / WA</label>
                <input type="tel" readonly name="no_hp" id="no_hp" class="form-control form-store" value="<?= $transactionMain["no_hp"] ?? ''; ?>" required>
              </div>
            </div>
            <div class="col-md-12" id="alamat">
              <div class="form-group">
                <label for="alamat">Alamat</label>
                <textarea name="alamat" readonly id="editor"><?= $transactionMain["alamat"] ?? ''; ?></textarea>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <hr>
              <h2 class="mb-2">Transfer Pelanggan</h2>
            </div>
            <div class="col-4">
              <div class="mb-4">
                Nama Pengirim : <strong><?= $transactionMain["nama"]; ?></strong>
              </div>
              <?php if ($transactionMain['foto_transaksiproduk'] == '') : ?>
                <p class="text-muted">Belum ada foto</p>
              <?php else : ?>
                <img src="../admin/bukti_transaksiproduk/<?= $transactionMain["foto_transaksiproduk"]; ?>" class="w-100" alt="">
              <?php endif; ?>

              <?php if ($transactionMain["status"] == "BELUM KONFIRMASI" && $transactionMain["foto_transaksiproduk"]) : ?>
                <form action="" method="POST">
                    <input type="hidden" name="id_transaksiproduk" value="<?= $transactionMain["id_transaksiproduk"]; ?>">
                    <button type="submit" onclick="return confirm('Apakah Ingin konfirmasi transaction ini ?')" name="konfirmasi" class="btn btn-success mt-4">Konfirmasi</button>
                </form>
              <?php endif;?>

              <?php if($transactionMain["status"] == "PICKUP" && $transactionMain["terkirim"] == 1) :  ?>
                <form action="" method="POST">
                    <input type="hidden" name="id_transaksiproduk" value="<?= $transactionMain["id_transaksiproduk"]; ?>">
                    <button type="submit" onclick="return confirm('Apakah Ingin Memproses Transaksi ini ?')" name="proses" class="btn btn-success mt-4">Proses</button>
                </form>
              
              <?php endif; ?>

              <?php if($transactionMain["status"] == "PROSES" && $transactionMain["terkirim"] == 1) :  ?>
                <form action="" method="POST">
                    <input type="hidden" name="id_transaksiproduk" value="<?= $transactionMain["id_transaksiproduk"]; ?>">
                    <button type="submit" onclick="return confirm('Apakah Ingin Memproses Transaksi  ini ?')" name="selesai" class="btn btn-success mt-4">Selesai</button>
                </form>
              
              <?php endif; ?>
            </div>
            <div class="col-4">
              Foto Penjemputan
              <?php if($transactionMain['foto_jemput'] == '') : ?>
                <p class="text-muted">Belum ada foto</p>
                <?php else : ?>
                  <img src="../admin/foto_jemput/<?= $transactionMain["foto_jemput"]; ?>" class="w-100" alt="">
                <?php endif; ?>
            </div>
            <div class="col-4">
              Foto Penerimaan
              <?php if($transactionMain['foto_penerimaan'] == '') : ?>
                <p class="text-muted">Belum ada foto</p>
                <?php else : ?>
                  <img src="../admin/foto_penerimaan/<?= $transactionMain["foto_penerimaan"]; ?>" class="w-100" alt="">
                <?php endif; ?>
            </div>
          </div>
          <div class="row" data-aos="fade-up" data-aos-delay="150">
            <div class="col-12">
              <hr>
            </div>
            <div class="col-12">
              <h2 class="mb-2">
                Information product
              </h2>
            </div>
          </div>
          <?php 
            $id_pembayaran = $transactionMain["id_pembayaran"];
            $rekening = query("SELECT * FROM pembayaran WHERE id_pembayaran = $id_pembayaran")[0];
          ?>
          <div class="row justify-content-center mb-5" data-aos="fade-up" data-aos-delay="200">
            <div class="col-4 col-md-3">
              <div class="font-weight-bold"><?= $jumlah; ?></div>
              <div class="text-muted">Banyak Barang</div>
            </div>
            <div class="col-4 col-md-3">
              <!-- <div class="font-weight-bold"><?= $transactionMain["weight_total"] / 1000; ?> Kilogram</div>
              <div class="text-muted">Berat Barang</div> -->
            </div>
            <div class="col-4 col-md-3">
              <?php if ($transactionMain["status"] == 'BELUM KONFIRMASI') : ?>
                <div class="text-danger font-weight-bold"><?= ($transactionMain["status"]); ?></div>
              <?php else : ?>
                <div class="text-success font-weight-bold"><?= ($transactionMain["status"]); ?></div>
              <?php endif;?>
              <div class="text-muted">Status Pembayaran</div>
            </div>
            <div class="col-4 col-md-3">
              <div class="text-success font-weight-bold">Rp. <?= number_format($transactionMain["total_harga"]); ?></div>
              <div class="text-muted">Total</div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  



