<?php 
$id = $_SESSION["user"];
$user = query("SELECT * FROM user WHERE id_user = $id")[0];

$id_transaksiproduk = $_GET["id_transaksiproduk"];
$transaction_details = query("SELECT * FROM detail_transaksiproduk INNER JOIN transaksi_produk ON detail_transaksiproduk.id_transaksiproduk = transaksi_produk.id_transaksiproduk INNER JOIN produk ON detail_transaksiproduk.id_produk = produk.id_Produk WHERE detail_transaksiproduk.id_transaksiproduk = $id_transaksiproduk");

if (isset($_POST["terkirim"])) {
  if (terkirim($_POST) > 0) {
    header("Location: ?page=transactions");
  }
}

?>
  <nav
  class="navbar navbar-expand-lg navbar-light navbar-store fixed-top"
  data-aos="fade-down"
>
  <div class="container-fluid">
    <button
      class="btn btn-secondary d-md-none mr-auto mr-2"
      id="menu-toggle"
    >
      &laquo; Menu
    </button>
    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarResponsive"
    >
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collpase navbar-collapse" id="navbarResponsive">
      <!-- dekstop menu -->
      <ul class="navbar-nav d-none d-lg-flex ml-auto">
        <li class="nav-item dropdown">
          <a
            href="#"
            class="nav-link"
            id="navbarDropdown"
            role="button"
            data-toggle="dropdown"
          >
          <img
              src="../assets/images/person-circle.svg"
              alt="profile"
              height="40px"
              class="rounded-circle mr-2 profile-picture"
            />
            <?php 
              $id_user = $_SESSION['user'];
              $user = query("SELECT * FROM user WHERE id_user = $id_user")[0];
            ?>
            Hi, <?= $user["nama"]; ?>
          </a>
          <div class="dropdown-menu">
            <a href="../index.php" class="dropdown-item">Back To Home</a>
            <div class="dropdown-divider"></div>
            <a href="../logout.php" class="dropdown-item">logout</a>
          </div>
        </li>
      </ul>

      <!-- mobile app -->
      <ul class="navbar-nav d-block d-lg-none">
        <li class="nav-item">
          <a href="" class="nav-link"> Hi, <?= $user["nama"]; ?></a>
        </li>
      </ul>
    </div>
  </div>
</nav>
  <div class="section-content section-dashboard-home" data-aos="fade-up">
    <div class="container-fluid">
      <div class="dashboard-heading">
      <?php 
        $transactionMain = query("SELECT * FROM transaksi_produk INNER JOIN user ON transaksi_produk.id_user = user.id_user WHERE transaksi_produk.id_transaksiproduk = $id_transaksiproduk")[0];
      ?>
        <h2 class="dashboard-title"><?= $transactionMain["nama"]; ?></h2>
        <p class="dashboard-subtitle">#<?= $transactionMain["code"]; ?></p>
      </div>
      <div class="dashboard-content store-cart">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body pb-0">
              <form action="" method="POST">
                  <div class="row" data-aos="fade-up" data-aos-delay="100">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="penerima">Nama Penerima</label>
                          <input type="hidden" name="id_transaksiproduk" value="<?= $transactionMain["id_transaksiproduk"]; ?>">
                          <input type="text" name="penerima" id="penerima" class="form-control">
                          <div class="text-right">
                            <button type="submit" name="terkirim" class="btn btn-success mt-4">Save Changes</button>
                          </div>
                        </div>
                      </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  



