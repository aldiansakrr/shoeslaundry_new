<?php 

if (isset($_POST["konfirmasi"])) {
  if (konfirmasi($_POST) > 0) {
    header("Location: ?page=transaksiproduk/transaksiproduk-no-confim");
  }
}

if (isset($_POST["terkirim"])) {
  if (terkirim($_POST) > 0) {
    header("Location: ?page=transaksiproduk/transaksiproduk-no-confim");
  }
}

?>
<nav
  class="navbar navbar-expand-lg navbar-light navbar-store fixed-top"
  data-aos="fade-down"
>
  <div class="container-fluid">
    <button
      class="btn btn-secondary d-md-none mr-auto mr-2"
      id="menu-toggle"
    >
      &laquo; Menu
    </button>
    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarResponsive"
    >
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collpase navbar-collapse" id="navbarResponsive">
      <!-- dekstop menu -->
      <ul class="navbar-nav d-none d-lg-flex ml-auto">
        <li class="nav-item dropdown">
          <a
            href="#"
            class="nav-link"
            id="navbarDropdown"
            role="button"
            data-toggle="dropdown"
          >
          <img
              src="../assets/images/person-circle.svg"
              alt="profile"
              height="40px"
              class="rounded-circle mr-2 profile-picture"
            />
            <?php 
              $id_user = $_SESSION['user'];
              $user = query("SELECT * FROM user WHERE id_user = $id_user")[0];
            ?>
            Hi, <?= $user["nama"]; ?>
          </a>
          <div class="dropdown-menu">
            <a href="../index.php" class="dropdown-item">Back To Home</a>
            <div class="dropdown-divider"></div>
            <a href="../logout.php" class="dropdown-item">logout</a>
          </div>
        </li>
      </ul>

      <!-- mobile app -->
      <ul class="navbar-nav d-block d-lg-none">
        <li class="nav-item">
          <a href="" class="nav-link"> Hi, <?= $user["nama"]; ?></a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<div class="section-content section-dashboard-home" data-aos="fade-up">
  <div class="container-fluid">
    <div class="dashboard-heading">
      <h2 class="dashboard-title">My Transaction</h2>
      <p class="dashboard-subtitle">This is menu for your transactions</p>
    </div>
    <div class="dashboard-content">
      <div class="row mt-4">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <?php if (isset($_SESSION["success"])) : ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                      <?= $_SESSION["success"]; ?>
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                  <?php endif;?>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table class="table table-striped table-hover" id="table">
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Code</th>
                          <th scope="col">Nama</th>
                          <th scope="col">Total</th>
                          <th scope="col">Pembayaran</th>
                          <th scope="col">Status</th>
                          <th scope="col">Penerima</th>
                          <th scope="col">Tanggal</th>
                          <th scope="col" class="text-center">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                          $no = 1;
                          $user_id = $_SESSION["user"];
                          $query = "SELECT * FROM transaksi_produk INNER JOIN user ON transaksi_produk.id_user = user.id_user INNER JOIN pembayaran ON transaksi_produk.id_pembayaran = pembayaran.id_pembayaran WHERE transaksi_produk.status = 'BELUM KONFIRMASI'";
                          $transactions = query($query);
                        ?>
                        <?php foreach ($transactions as $transaction) : ?>
                          <tr>
                            <th scope="row"><?= $no; ?></th>
                            <td>#<?= $transaction["code"]; ?></td>
                            <td><?= $transaction["nama"]; ?></td>
                            <td><?= number_format($transaction["total_harga"]); ?></td>
                            <td><?= $transaction["nama_pembayaran"]; ?></td>
                            <td>
                              <?php if ($transaction["status"] == "BELUM KONFIRMASI") : ?>
                                <span class="badge badge-pill badge-danger"><?= $transaction["status"]; ?></span>
                              <?php elseif($transaction["status"] == "TERKONFIRMASI"): ?>
                              <span class="badge badge-pill badge-warning"><?= $transaction["status"]; ?></span>
                              <?php elseif($transaction["status"] == "PICKUP") : ?>
                                <span class="badge badge-pill badge-primary"><?= $transaction["status"]; ?></span>
                              <?php else: ?>
                                <span class="badge badge-pill badge-success"><?= $transaction["status"]; ?></span>
                              <?php endif; ?>
                            </td>
                            <?php if (isset($transaction["penerima"])) : ?>
                              <td><?= $transaction["penerima"] ? $transaction['penerima'] : 'Belum Diterima'; ?></td>
                            <?php endif;?>
                            <?php 
                              $tanggal = $transaction["created_at"];
                            ?>
                            <td><?= date('d, F Y', strtotime($tanggal)); ?></td>
                            <td style="width: 17%; text-align: center;">
                              <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Aksi
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                  <a class="dropdown-item" href="?page=transaksiproduk/transaksiproduk-details&id_transaksiproduk=<?= $transaction["id_transaksiproduk"]; ?>">Lihat</a>
                                  <?php if($transaction["status"] == "TERKONFIRMASI" && $transaction["diterima"] == 0): ?>
                                    <a href="?page=transaksiproduk/transaksiproduk-receiver&id_transaksiproduk=<?= $transaction["id_transaksiproduk"]; ?>" class="dropdown-item">terima</a>
                                  <?php endif; ?>
                                  <a class="dropdown-item" onclick="return confirm('Apakah Ingin Menghapus transaction ini ?')" href="?page=transaksiproduk/transaksiproduk-delete&id_transaksiproduk=<?= $transaction["id_transaksiproduk"]; ?>">Delete</a>
                                </div>
                              </div>
                            </td>
                          </tr>
                          <?php $no++ ?>
                        <?php endforeach;?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
