<?php
$page = "dashboard";
require_once '../config/config.php';
if (isset($_SESSION["login"]) && isset($_SESSION["driver"])) {
  header("Location: ../driver/index.php");
}
if (!isset($_SESSION["login"]) && !isset($_SESSION["user"])) {
  header("Location: ../index.php");
} else {
  $id = $_SESSION["user"];
  $result = query("SELECT * FROM user WHERE id_user = $id")[0];
  if ($result['role'] !== 'ADMIN') {
    header("Location: ../index.php");
  }
}

if (isset($_POST["terkirim"])) {
  if (terkirim($_POST) > 0) {
    header("Location: ?page=transactions");
  }
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>Dashboard Admin | Clean Shoes</title>

    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />
    <link href="../assets/style/main.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../assets/vendor/DataTables/datatables.min.css"/>
    <link rel="icon" type="image/png" href="../gambar/logo.png">
    <style>
      .dropdown-toggle:focus {
        outline-style: none;
      }
    </style>
  </head>

  <body>
    <?php 
    if (isset($_GET["page"])) {
      $page = $_GET["page"];
    }
    ?>
    <div class="page-dashboard">
      <div class="d-flex" id="wrapper" data-aos="fade-right">
        <!-- sidebar -->
        <div class="border-right" id="sidebar-wrapper">
          <div class="sidebar-heading text-center">
            <img src="../gambar/logo.png" alt="" class="my-4 w-50" />
          </div>
          <div class="list-group list-group-flush">
            <a
              href="?page=dashboard"
              class="list-group-item list-group-item-action<?= $page == 'dashboard' ? ' active' : ''; ?> <?= $page == '' ? ' active' : ''; ?>"
            >
              Dashboard
            </a>
            <a
              href="?page=products"
              class="list-group-item list-group-item-action<?= $page == 'products' ? ' active' : ''; ?> <?= $page == 'products-create' ? ' active' : ''; ?> <?= $page == 'products-details' ? ' active' : ''; ?>"
            >
              Products
            </a>
            <a
              href="?page=jasa"
              class="list-group-item list-group-item-action<?= $page == 'jasa' ? ' active' : ''; ?> <?= $page == 'jasa-create' ? ' active' : ''; ?> <?= $page == 'jasa-details' ? ' active' : ''; ?>"
            >
              Jasa
            </a>
            <a
              href="?page=pembayaran"
              class="list-group-item list-group-item-action<?= $page == 'pembayaran' ? ' active' : ''; ?> <?= $page == 'pembayaran-create' ? ' active' : ''; ?> <?= $page == 'pembayaran-details' ? ' active' : ''; ?>"
            >
              Pembayaran
            </a>
            <!-- <a
              href="?page=categories"
              class="list-group-item list-group-item-action<?= $page == 'categories' ? ' active' : ''; ?> <?= $page == 'categories-create' ? ' active' : ''; ?> <?= $page == 'categories-details' ? ' active' : ''; ?> <?= $page == 'categories-delete' ? ' active' : ''; ?>"
            >
              Categories
            </a> -->
            <div class="dropdown">
              <button class="list-group-item list-group-item-action  
              <?php 
              if($page=="transaksiproduk" || 
              $page=="transaksiproduk/transaksiproduk-no-confirm" || 
              $page=="transaksiproduk/transaksiproduk-confirm" ||
              $page=="transaksiproduk/transaksiproduk-pickup" ||
              $page=="transaksiproduk/transaksiproduk-sent") { echo "active"; } ?>
              dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Transaksi Produk
              </button>
              <div class="dropdown-menu<?php 
              if($page=="transaksiproduk" || 
              $page=="transaksiproduk/transaksiproduk-no-confirm" ||
              $page=="transaksiproduk/transaksiproduk-confirm" ||
              $page=="transaksiproduk/transaksiproduk-pickup" ||
              $page=="transaksiproduk/transaksiproduk-sent"){ echo "-open"; } ?>" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item<?php if($page=="transaksiproduk"){ echo " active"; } ?>" href="?page=transaksiproduk">All Transactions</a>
                <a class="dropdown-item<?php if($page=="transaksiproduk/transaksiproduk-no-confirm"){ echo " active"; } ?>" href="?page=transaksiproduk/transaksiproduk-no-confirm">Belum Konfirmasi</a>
                <a class="dropdown-item<?php if($page=="transaksiproduk/transaksiproduk-confirm"){ echo " active"; } ?>" href="?page=transaksiproduk/transaksiproduk-confirm">Konfirmasi</a>
                <a class="dropdown-item<?php if($page=="transaksiproduk/transaksiproduk-pickup"){ echo " active"; } ?>" href="?page=transaksiproduk/transaksiproduk-pickup">Pick Up</a>
                <a class="dropdown-item<?php if($page=="transaksiproduk/transaksiproduk-sent"){ echo " active"; } ?>" href="?page=transaksiproduk/transaksiproduk-sent">Terkirim</a>
              </div>
            </div>
            <a
              href="?page=pegawai"
              class="list-group-item list-group-item-action<?= $page == 'pegawai' ? ' active' : ''; ?> <?= $page == 'pegawai-create' ? ' active' : ''; ?> <?= $page == 'pegawai-details' ? ' active' : ''; ?> <?= $page == 'pegawai-delete' ? ' active' : ''; ?>"
            >
              Pegawai
            </a>
            <a
              href="?page=users"
              class="list-group-item list-group-item-action<?= $page == 'users' ? ' active' : ''; ?> <?= $page == 'users-create' ? ' active' : ''; ?> <?= $page == 'users-details' ? ' active' : ''; ?>"
            >
              Users
            </a>
            <a
              href="?page=logout"
              class="list-group-item list-group-item-action<?= $page == 'logout' ? ' active' : ''; ?>"
            >
              Sign Out
            </a>
          </div>
        </div>

        <!-- page content -->
        <div id="page-content-wrapper">
          <?php 

          if (isset($page)) {
            if ($page == 'dashboard') {
              include 'dashboard.php';
            } elseif ($page == 'products') {
              include 'dashboard-products.php';
            } elseif ($page == 'products-create') {
              include 'dashboard-product-create.php';
            } elseif ($page == 'products-details') {
              include 'dashboard-products-details.php';
            } elseif ($page == 'products-delete') {
              include 'dashboard-product-delete.php';
            } elseif ($page == 'jasa') {
              include 'jasa/dashboard-jasa.php';
            } elseif ($page == 'jasa-create') {
              include 'jasa/dashboard-jasa-create.php';
            } elseif ($page == 'jasa-details') {
              include 'jasa/dashboard-jasa-details.php';
            } elseif ($page == 'jasa-delete') {
              include 'jasa/dashboard-jasa-delete.php';
            } elseif ($page == 'pembayaran') {
              include 'dashboard-pembayaran.php';
            } elseif ($page == 'pembayaran-create') {
              include 'dashboard-pembayaran-create.php';
            } elseif ($page == 'pembayaran-details') {
              include 'dashboard-pembayaran-details.php';
            } elseif ($page == 'pembayaran-delete') {
              include 'dashboard-pembayaran-delete.php';
            } elseif ($page == 'transaksiproduk') {
              include 'transaksiproduk/dashboard-transaksiproduk.php';
            } elseif ($page == 'transaksiproduk/transaksiproduk-receiver') {
              include 'transaksiproduk/dashboard-transaksiproduk-receiver.php';
            } elseif ($page == 'transaksiproduk/transaksiproduk-no-confirm') {
              include 'transaksiproduk/dashboard-transaksiproduk-no-confirm.php';
            } elseif ($page == 'transaksiproduk/transaksiproduk-confirm') {
              include 'transaksiproduk/dashboard-transaksiproduk-confirm.php';
            } elseif ($page == 'transaksiproduk/transaksiproduk-pickup') {
              include 'transaksiproduk/dashboard-transaksiproduk-pickup.php';
            } elseif ($page == 'transaksiproduk/transaksiproduk-sent') {
              include 'transaksiproduk/dashboard-transaksiproduk-sent.php';
            } elseif ($page == 'transaksiproduk/transaksiproduk-details') {
              include 'transaksiproduk/dashboard-transaksiproduk-details.php';
            } elseif ($page == 'transaksiproduk/transaksiproduk-delete') {
              include 'transaksiproduk/dashboard-transaksiproduk-delete.php';
            } elseif ($page == 'transaksiproduk/transaksiproduk-proses') {
              include 'transaksiproduk/dashboard-transaksiproduk-proses.php';
            } elseif ($page == 'pegawai') {
              include 'pegawai/dashboard-pegawai.php';
            } elseif ($page == 'pegawai-create') {
              include 'pegawai/dashboard-pegawai-create.php';
            } elseif ($page == 'pegawai-details') {
              include 'pegawai/dashboard-pegawai-details.php';
            } elseif ($page == 'pegawai-delete') {
              include 'pegawai/dashboard-pegawai-delete.php';
            } elseif ($page == 'users') {
              include 'users/dashboard-users.php';
            } elseif ($page == 'users-create') {
              include 'users/dashboard-users-create.php';
            } elseif ($page == 'users-details') {
              include 'users/dashboard-users-details.php';
            } elseif ($page == 'users-delete') {
              include 'users/dashboard-users-delete.php';
            } elseif ($page == 'logout') {
              include '../logout.php';
            } else {
              
              echo "Halaman Tidak Ditemukan";
            }
          } else {
            include 'dashboard.php';
          }

          ?>
        </div>
      </div>
    </div>

    

    <!-- Bootstrap core JavaScript -->
    <script src="../assets/vendor/jquery/jquery.slim.min.js"></script>
    <script src="../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
      AOS.init();
    </script>
    <script>
      $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
      });
    </script>
    <script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    <script>
      CKEDITOR.replace("editor");
    </script>
    <script type="text/javascript" src="../assets/vendor/DataTables/datatables.min.js"></script>
    <script>
      $(document).ready(function() {
          $('#table').DataTable();
      } );
    </script>
  </body>
</html>
