<?php 

$id_jasa = $_GET["id_jasa"];

$jasa = query("SELECT * FROM jasa WHERE id_jasa = $id_jasa")[0];

if (isset($_POST["updateJasa"])) {
  if (updateJasa($_POST) > 0) {
    echo "<script>
            alert('Jasa Berhasil Diubah');
            document.location.href = '?page=jasa';
          </script>";
  } else {
    echo "<script>
            alert('Jasa Gagal Diubah');
            document.location.href = '?page=jasa';
          </script>";
  }
}

?>
<nav
  class="navbar navbar-expand-lg navbar-light navbar-store fixed-top"
  data-aos="fade-down"
>
  <div class="container-fluid">
    <button
      class="btn btn-secondary d-md-none mr-auto mr-2"
      id="menu-toggle"
    >
      &laquo; Menu
    </button>
    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarResponsive"
    >
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collpase navbar-collapse" id="navbarResponsive">
      <!-- dekstop menu -->
      <ul class="navbar-nav d-none d-lg-flex ml-auto">
        <li class="nav-item dropdown">
          <a
            href="#"
            class="nav-link"
            id="navbarDropdown"
            role="button"
            data-toggle="dropdown"
          >
            <img
              src="../assets/images/person-circle.svg"
              alt="profile"
              height="40px"
              class="rounded-circle mr-2 profile-picture"
            />
            <?php 
              $id_user = $_SESSION['user'];
              $user = query("SELECT * FROM user WHERE id_user = $id_user")[0];
            ?>
            Hi, <?= $user["nama"]; ?>
          </a>
          <div class="dropdown-menu">
            <a href="../index.php" class="dropdown-item">Back To Home</a>
            <div class="dropdown-divider"></div>
            <a href="../logout.php" class="dropdown-item">logout</a>
          </div>
        </li>
      </ul>

      <!-- mobile app -->
      <ul class="navbar-nav d-block d-lg-none">
        <li class="nav-item">
          <a href="" class="nav-link"> Hi, <?= $user["nama"]; ?></a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<div
  class="section-content section-dashboard-home"
  data-aos="fade-up"
>
  <div class="container-fluid">
    <div class="dashboard-heading">
      <h2 class="dashboard-title"><?= $jasa["nama_jasa"]; ?></h2>
      <p class="dashboard-subtitle">Jasa Details</p>
    </div>
    <div class="dashboard-content">
      <div class="row">
        <div class="col-12 mt-2">
          <form action="" method="POST">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                  <input type="hidden" value="<?= $jasa["id_jasa"]; ?>" name="id_jasa">
                    <div class="form-group">
                      <label for="nama_jasa">Nama Jasa</label>
                      <input
                        type="text"
                        name="nama_jasa"
                        id="nama_jasa"
                        class="form-control"
                        value="<?= $jasa["nama_jasa"]; ?>"
                      />
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="harga_jasa">Harga Jasa</label>
                      <input
                        type="number"
                        name="harga_jasa"
                        id="price"
                        class="form-control"
                        min="0"
                        value="<?= $jasa["harga_jasa"]; ?>"
                      />
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="jenis_jasa">Jenis Jasa</label>
                      <select name="jenis_jasa" id="jenis_jasa" class="form-control">
                        <option value="Cleaning" <?php if($jasa["jenis_jasa"]=="Cleaning"){ echo "selected"; } ?> >Cleaning</option>
                        <option value="Repaint" <?php if($jasa["jenis_jasa"]=="Repaint"){ echo "selected"; } ?> >Repaint</option>
                        <option value="Others" <?php if($jasa["jenis_jasa"]=="Others"){ echo "selected"; } ?> >Others</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Deskripsi Jasa</label>
                      <textarea name="deskripsi_jasa" id="editor"><?= $jasa["deskripsi_jasa"]; ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="row my-3">
                  <div class="col-12">
                    <button
                      type="submit"
                      name="updateJasa"
                      class="btn btn-success btn-block py-2"
                    >
                      Update Jasa
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="card my-4">
              <div class="card-body">
                <div class="row mt-2">
                  <div class="col-md-4 mb-3">
                    <div class="gallery-container">
                      <img
                        src="../assets/images/1.jpg"
                        alt=""
                        class="w-100"
                      />
                      <a href="#" class="delete-gallery">
                        <img
                          src="../assets/images/icon-delete.svg"
                          alt=""
                        />
                      </a>
                    </div>
                  </div>
                  <div class="col-md-4 mb-3">
                    <div class="gallery-container">
                      <img
                        src="../assets/images/2.jpg"
                        alt=""
                        class="w-100"
                      />
                      <a href="#" class="delete-gallery">
                        <img
                          src="../assets/images/icon-delete.svg"
                          alt=""
                        />
                      </a>
                    </div>
                  </div>
                  <div class="col-md-4 mb-3">
                    <div class="gallery-container">
                      <img
                        src="../assets/images/3.jpg"
                        alt=""
                        class="w-100"
                      />
                      <a href="#" class="delete-gallery">
                        <img
                          src="../assets/images/icon-delete.svg"
                          alt=""
                        />
                      </a>
                    </div>
                  </div>
                </div>
                <div class="row my-3">
                  <div class="col-12">
                    <input
                      type="file"
                      name="file"
                      id="file"
                      style="display: none"
                      multiple
                    />
                    <button
                      type="button"
                      name="save"
                      class="btn btn-secondary btn-block py-2"
                      onclick="thisFileUpload()"
                    >
                      Add Photo
                    </button>
                  </div>
                </div>
              </div>
            </div> -->
          </form>
        </div>
      </div>
    </div>
  </div>
</div>